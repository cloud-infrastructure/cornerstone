================
Cornerstone API
================

The API is a designed to be a SOAP service allowing integration
from FIM to do operations on keystone. It will be accessed through
.NET Framework as long as a small client utility

Default Return Codes
====================

All the operation of the SOAP interface will return a code that
indicates if the operation was successful or not (and the reason)

=====  =====================================================
Code   Reason 
=====  =====================================================
  -1   An exception has been produced in the server
  0    Command executed successfully
  1    There is an error in the validation of the parameters
  2    Project already exists
=====  =====================================================


Functions
=========

add_project_members
-------------------
This function adds the members on the project within the role

Parameters
  :id:  Identifier in the project
  :rolename: Name of the role
  :members: Coma-separated member id's (users and/groups)

Return:
  ReturnCode
    :returnCode: code returned after doing the operation

del_project_members
-------------------
This function removes the members on the project within the role

Parameters
  :id:  Identifier in the project
  :rolename: Name of the role
  :members: Coma-separated member id's (users and/groups)

Return:
  ReturnCode
    :returnCode: code returned after doing the operation

get_project_members
-------------------
This function adds the members on the project within the role

Parameters
  :id:  Identifier in the project
  :rolename: Name of the role

Return:
  ReturnMemberList
    :returnCode: code returned after doing the operation
    :ProjectList: member list retrieved if successful, otherwise none

set_project_members
-------------------
This function sets the members on the project within the role

Parameters
  :id:  Identifier in the project
  :rolename: Name of the role
  :members: Coma-separated member id's (users and/groups)

Return:
  ReturnCode
    :returnCode: code returned after doing the operation

project_create
--------------
This function allows to create a project on the infrastructure. It will
create the project in keystone and create some default security rules
in nova as well.

Parameters
  :id:  Identifier in the project
  :name: Name of the project
  :description: Description of the project
  :enabled: Enable/Disable the project
  :owner: Owner of the project

Return:
  ReturnProject
    :returnCode: code returned after doing the operation
    :Project: project created if successful, otherwise None


project_delete
--------------
This function allows to delete a project on the infrastructure. It will
delete the project in keystone. It will also remove all the resources
assigned to it.

Parameters
  :id:  Identifier of the project to delete

Return
  ReturnProject
    :returnCode: code returned after doing the operation
    :Project: None


project_update
--------------
This function allows to update a project on the infrastructure. It will
update the project in keystone. If the project is going to be disable, it
will stop all the resources assigned to it.

Parameters
  :id:  Identifier of the project to update
  :name: New name of the project
  :description: New description of the project
  :enabled: Enable/Disable the project
  :owner: New owner of the project

Return
  ReturnProject
    :returnCode: code returned after doing the operation
    :Project: project updated if successful, otherwise none


project_get
-----------
This function allows to retrieve a project on the infrastructure. It will
return the project in keystone identified

Parameters
  :id:  Identifier of the project to retrieve

Return
  ReturnProject
    :returnCode: code returned after doing the operation
    :Project: project retrieved if successful, otherwise none


project_getInfo
---------------
This function allows to retrieve a project on the infrastructure. It will
return the project in keystone identified with extra information like
sysadmin access

Parameters
  :id:  Identifier of the project to retrieve

Return
  ReturnProject
    :returnCode: code returned after doing the operation
    :Project: project retrieved if successful, otherwise none


project_list
------------
This function allows to retrieve all the projects on the infrastructure.
It will return a list of the projects in keystone

Return
  ReturnProjectList
    :returnCode: code returned after doing the operation
    :ProjectList: project list retrieved if successful, otherwise none


create_security_groups
----------------------
This function create the default security groups on a given project.

Parameters
  :id:  Identifier in the project

Return:
  ReturnCode
    :returnCode: code returned after doing the operation


set_allowOperators
------------------
This function sets the operators access on the project

Parameters
  :id:  Identifier in the project
  :value: Boolean flag indicating operators access

Return:
  ReturnCode
    :returnCode: code returned after doing the operation
