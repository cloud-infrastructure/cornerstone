========================================================================
Welcome to Cornerstone, FIM integration with OpenStack Identity Service!
========================================================================

Cornerstone is a project to integrate FIM into the OpenStack Identity Service

This documentation is generated by the Sphinx toolkit and lives in the source
tree.  Additional documentation on Cornerstone and other components of Agile 
can be found on the `Agile Twiki`_.

.. _`Agile Twiki`: https://twiki.cern.ch/twiki/bin/view/AgileInfrastructure

Developers Documentation
========================
.. toctree::
   :maxdepth: 1

   api
   releases

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

