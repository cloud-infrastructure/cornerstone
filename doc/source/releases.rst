=============
Release notes
=============

1.1 (Oct 01, 2015)
==================
- OS-2148 Improve get group in addRoleAssignment function
- OS-2149 Set keystone_timeout as float
- OS-2150 Logging improvements related with FIM
- Remove groups.list calls
- OS-1689 Cornerstone notifications to keystone for project creation
- OS-2123 Change all DEBUG by INFO
- OS-2124 Raise proper exception when eGroup is not found in the filter
- OS-1879-2/2077 FIX Check against ldap before send request to egroups app
- OS-2060 display_name not valid in Cinder V2
- OS-2113 Check status server before detach volume
- OS-2118 nova_timeout should be float
- OS-1879-2 FIX Check against ldap before send request to egroups app
- OS-1593 Add REST API to cornerstone
- OS-1651 Project created without owner for non-existing user
- OS-1658 Add REST API to cornerstoneclient
- OS-1674 Improve INFO logging mode
- OS-1675 Add confirmation delete in cornerstoneclient
- OS-1678 Add logging to cornerstoneclient
- OS-1764 Fix tests
- OS-1791 Some volume types quotas not set properly
- OS-1796 Add REST Properties call
- OS-1797 Add cornerstone-properties
- OS-1873 Check against ldap before send request to egroups app
- OS-1893 Change cinder calls to V2
- OS-1914 Add fim-lock and fim-skip by default to new shared projects
- OS-1936 Don't stop VMs with status!=active
- OS-1747 Not stop VM's in suspend/pause state
- OS-2007 project-members-set remove the project owner

1.0 (May 14, 2014)
==================
- First version
- Complete refactor
- Test environment
- Change to v3 keystone api

0.9 (March 12, 2014)
====================
- CRUD on role assignment for projects
- Improved lifecycle management for projects
- remove unused functionality

0.5 (February 21, 2014)
=======================
- Allow accounting and operators roles
- Upgrade keystone connections to use v3

0.4 (October 23, 2013)
======================
- FIM Policies for NOVA and GLANCE

0.3 (July 29, 2013)
===================
- Introduce ownership management in projects
- Added interaction with egroups application
- Added egroup management for different account types

0.2 (March 15, 2013)
====================
* Added more configuration parameters

0.1 (March 07, 2013)
====================
* First version of the API server 
