cornerstone
===========

[![pipeline status](https://gitlab.cern.ch/cloud-infrastructure/cornerstone/badges/master/pipeline.svg)](https://gitlab.cern.ch/cloud-infrastructure/cornerstone/-/commits/master)
[![coverage report](https://gitlab.cern.ch/cloud-infrastructure/cornerstone/badges/master/coverage.svg)](https://gitlab.cern.ch/cloud-infrastructure/cornerstone/-/commits/master)

Cornerstone is a python project to allow the integration of the Cloud Service at CERN
