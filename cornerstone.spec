Name:           cornerstone
Version:        2.2.2
Release:        1%{?dist}
Summary:        Python API and CLI for FIM integration in keystone

License:        ASL 2.0
BuildArch:      noarch
URL:            https://gitlab.cern.ch/cloud-infrastructure/cornerstone
Source0:        %{name}-%{version}.tar.gz

BuildRequires:  python3-devel
BuildRequires:  python3-pbr
BuildRequires:  python3-setuptools
BuildRequires:  make
BuildRequires:  systemd

Requires:       python3-cornerstone = %{version}-%{release}
Requires:       python3-setuptools

%description
Server API for FIM integration.
This package contains the configuration files

%package -n	python3-cornerstone
Summary:        Cornerstone Python libraries

Requires:       python3-django
Requires:       python3-django-rest-framework
Requires:       python3-keystoneclient
Requires:       python3-mistralclient
Requires:       httpd
Requires:       python3-mod_wsgi
Requires:       python3-pyws
Requires:       python3-PyMySQL
Requires:       python3-zeep
Requires:       python3-requests
Requires:       python3-mock

%description -n   python3-cornerstone
Cornerstone is a Python implementation of the API for FIM integration.

%package doc
Summary:	Documentation for Cornerstone

BuildRequires:	python3-sphinx


%description doc
Cornerstone is a Python implementation of the API for FIM integration.

This package contains documentation for Cornerstone.

%prep
%autosetup -p1 -n %{name}-%{version}

# Fix for shebangs
pathfix.py -pni "%{__python3} %{py3_shbang_opts}" .

%build
%{py3_build}

%install
%{py3_install}

install -d -m 755 %{buildroot}%{_sysconfdir}/cornerstone
install -p -D -m 640 etc/cornerstone.conf %{buildroot}%{_sysconfdir}/httpd/conf.d/cornerstone.conf
install -d -m 755 %{buildroot}%{_localstatedir}/log/cornerstone
install -d -m 755 %{buildroot}%{_datadir}/cornerstone
install -d -m 755 %{buildroot}%{_datadir}/cornerstone/static

# Move config to /etc, symlink it back to /usr/share
mv %{buildroot}%{python3_sitelib}/cornerstone/local/local_settings.example.py %{buildroot}%{_sysconfdir}/cornerstone/local_settings
ln -s ../../../../../..%{_sysconfdir}/cornerstone/local_settings %{buildroot}%{python3_sitelib}/cornerstone/local/local_settings.py

# create directory for systemd snippet
mkdir -p %{buildroot}%{_unitdir}/httpd.service.d/
cp etc/cornerstone-systemd.conf %{buildroot}%{_unitdir}/httpd.service.d/cornerstone.conf

# docs generation requires everything to be installed first
export PYTHONPATH="$( pwd ):$PYTHONPATH"
sphinx-build -b html doc/source html

# Fix hidden-file-or-dir warnings
rm -fr html/.doctrees html/.buildinfo

%files
%dir %attr(0755, apache, apache) %{_sysconfdir}/cornerstone
%config(noreplace) %attr(0644, root, apache) %{_sysconfdir}/cornerstone/local_settings
%config(noreplace) %attr(0644, root, apache) %{_sysconfdir}/httpd/conf.d/cornerstone.conf
%dir %attr(0755, apache, apache) %{_localstatedir}/log/cornerstone

%files -n python3-cornerstone
%{python3_sitelib}/cornerstone
%dir %attr(0755, apache, apache) %{_datadir}/cornerstone
%dir %attr(0755, apache, apache) %{_datadir}/cornerstone/static
%attr(0755, root, root) %{python3_sitelib}/cornerstone/manage.py
%{python3_sitelib}/cornerstone-*-py*.egg-info/
%config(noreplace) %{_unitdir}/httpd.service.d/cornerstone.conf

%post
systemctl daemon-reload >/dev/null 2>&1 || :

%postun
# update systemd unit files
systemctl daemon-reload >/dev/null 2>&1 || :

%files doc
%doc html

%changelog
* Mon Feb 10 2025 Jose Castro Leon <jose.castro.leon@cern.ch> 2.2.2-1
- Add missing id field for serializer on updates

* Tue Jan 21 2025 Jose Castro Leon <jose.castro.leon@cern.ch> 2.2.1-5
- Add missing id field for serializer on updates

* Tue Jan 21 2025 Jose Castro Leon <jose.castro.leon@cern.ch> 2.2.1-4
- Allow serializers to process entries with None or empty values

* Thu Jan 16 2025 Jose Castro Leon <jose.castro.leon@cern.ch> 2.2.1-3
- Fix bug with status and type

* Tue Jan 14 2025 Jose Castro Leon <jose.castro.leon@cern.ch> 2.2.1-2
- Add administrators field and also increase test coverage

* Mon Jan 13 2025 Jose Castro Leon <jose.castro.leon@cern.ch> 2.2.1-1
- Add administrator group in all apis up to driver calls

* Mon Feb 26 2024 Jose Castro Leon <jose.castro.leon@cern.ch> 2.2.0-7
- Add envelope for the data sent back

* Fri Feb 23 2024 Jose Castro Leon <jose.castro.leon@cern.ch> 2.2.0-6
- Remove Person object from rest/v2 api

* Mon Feb 12 2024 Jose Castro Leon <jose.castro.leon@cern.ch> 2.2.0-5
- Fix issue with project_create and project_update after adding additional attributes

* Mon Feb 12 2024 Jose Castro Leon <jose.castro.leon@cern.ch> 2.2.0-4
- Remove staticfiles in cornerstone

* Fri Feb 09 2024 Jose Castro Leon <jose.castro.leon@cern.ch> 2.2.0-3
- Add missing static folder in share

* Wed Feb 07 2024 Jose Castro Leon <jose.castro.leon@cern.ch> 2.2.0-2
- Use project_get instead of project_getinfo in v2 api

* Wed Feb 07 2024 Jose Castro Leon <jose.castro.leon@cern.ch> 2.2.0-1
- Initial implementation of v2 API
- Fix rest framework static configuration
- Refactor drivers to allow other workflow engines in the future
- Add additional configuration settings to fine tune the workflows

* Mon Dec 18 2023 Jose Castro Leon <jose.castro.leon@cern.ch> 2.1.4-4
- Fix issue while loading template for schema
- Handle cases in which the logic does not return

* Wed Dec 13 2023 Jose Castro Leon <jose.castro.leon@cern.ch> 2.1.4-3
- Fix issue while loading template for schema

* Tue Nov 28 2023 Jose Castro Leon <jose.castro.leon@cern.ch> 2.1.4-2
- Increase the version to offer support for MySQLdb on PyMySQL

* Wed Nov 22 2023 Jose Castro Leon <jose.castro.leon@cern.ch> 2.1.4-1
- Rebuild for rhel9 and alma9

* Mon Apr 24 2023 Jose Castro Leon <jose.castro.leon@cern.ch> 2.1.3-2
- Rebuild for rhel8 and alma8

* Tue Oct 05 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 2.1.3-1
- Build only on el8s
- Fix issue with wf_identifier in new mistral clients
- Remove ldap dependency as it is not used anymore

* Tue Apr 13 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 2.1.2-2
- Rebuild for el8s

* Mon Jan 18 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 2.1.2-1
- Fix issue while running on Django 2.2.9

* Wed Jun 17 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 2.1.1-11
- Add dependency on mod_wsgi for el7 and python3 version for el8

* Wed Jun 17 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 2.1.1-10
- Replace MySQLdb by PyMYSQL
- Fix local import from settings file
- Add pymysql dependency in requirements.txt

* Fri May 15 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 2.1.1-9
- Rebuild compatible with el7 and el8

* Mon Nov 18 2019 Jose Castro Leon <jose.castro.leon@cern.ch> 2.1.1-8
- Fix schemas to match requirements from new resources

* Thu Nov 15 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 2.1.1-7
- Fix issue on asynchronous workflow execution
- Removal of unused code in keystoneclient
- Move MISTRAL_PROJECT variable to ADMIN_PROJECT
- Cleanup of unused code and configuration settings

* Wed Nov 14 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 2.1.1.6
- Fix some issues in execution of member and allow service workflows

* Mon Nov 12 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 2.1.1-5
- Fix unexpected signature change for SOAP clients

* Fri Nov 09 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 2.1.1-4
- Remove some extra attributes on project get

* Thu Nov 08 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 2.1.1-3
- Try to not reauthenticate
- Fix behaviour with new mistral client in rocky
- Fix issue on project get in SOAP

* Tue Nov 06 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 2.1.1-2
- Change requirements to python2-django and python2-django-rest-framework

* Tue Nov 06 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 2.1.1-1
- Fix issues on django 1.11 and djangorestframework

* Mon Oct 22 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 2.1.0-1
- Move ACLS and property management to mistral workflows

* Thu Feb 22 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 2.0.0-5
- Fix bug on hardcoded services entries

* Fri Feb 16 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 2.0.0-4
- Remove hardcoded services from code and rely on API

* Thu Feb 15 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 2.0.0-3
- Fix lint issues and remove unused code

* Wed Feb 14 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 2.0.0-1
- Remove business logic that is now in mistral workflows
- Remove unused clients and configuration
- Major refactor of the code, simplify settings and internal packages

* Tue Feb 13 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 1.6-16
- Fix another bug in endpoint group filtering
- Make allow_service synchronous

* Tue Feb 13 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 1.6-15
- Fix bug introduced on removing sql_utils
- Remove manual path in code

* Tue Feb 13 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 1.6-14
- Remove usage of sql_utils as it is not needed anymore

* Mon Feb 12 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 1.6-13
- Remove duplicated code on REST and SOAP calls
- Remove description parameters in launch workflow

* Fri Feb 09 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 1.6-12
- Move business logic inside the command manager

* Fri Feb 09 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 1.6-11
- Improving project_delete workflow and error handling

* Fri Feb 09 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 1.6-10
- Reduce logging on urllib3 and requests
- Fix issues on workflows executed synchronously

* Thu Feb 01 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 1.6-9
- Simplify service handling with mistral

* Wed Jan 31 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 1.6-8
- Simplify execution of mistral workflows

* Tue Oct 31 2017 Jose Castro Leon <jose.castro.leon@cern.ch> 1.6-7
- Fix bug on volume deletion

* Tue Oct 24 2017 Jose Castro Leon <jose.castro.leon@cern.ch> 1.6-6
- Add support for supporters call

* Thu Oct 19 2017 Jose Castro Leon <jose.castro.leon@cern.ch> 1.6-5
- Quotas cleanup on project deletion

* Mon Oct 16 2017 Jose Castro Leon <jose.castro.leon@cern.ch> 1.6-4
- Replace suds by zeep

* Mon Oct 16 2017 Jose Castro Leon <jose.castro.leon@cern.ch> 1.6-3
- Fix bugs on accounting removal

* Mon Oct 16 2017 Jose Castro Leon <jose.castro.leon@cern.ch> 1.6-2
- Change order in project create
- Add missing wait on project creation

* Mon Sep 25 2017 Jose Castro Leon <jose.castro.leon@cern.ch> 1.6-1
- Remove accounting handling as it is not used anymore

* Tue Aug 15 2017 Jose Castro Leon <jose.castro.leon@cern.ch> 1.6-0
- Add support for mistral workflows

* Tue Aug 15 2017 Jose Castro Leon <jose.castro.leon@cern.ch> 1.5-5
- Fix some bugs on service list

* Mon Aug 14 2017 Jose Castro Leon <jose.castro.leon@cern.ch> 1.5-4
- Fix some bugs on quota requests on cinder

* Mon Aug 14 2017 Jose Castro Leon <jose.castro.leon@cern.ch> 1.5-3
- Rewrite configuration handling and simplify more the codebase

* Thu Aug 10 2017 Jose Castro Leon <jose.castro.leon@cern.ch> 1.5-2
- Fix another set of bugs in project CRUD

* Thu Aug 10 2017 Jose Castro Leon <jose.castro.leon@cern.ch> 1.5-1
- Fix bugs in general

* Thu Aug 10 2017 Jose Castro Leon <jose.castro.leon@cern.ch> 1.5-0
- Major simplification of the code

* Tue Jul 25 2017 Jose Castro Leon <jose.castro.leon@cern.ch> 1.4-9
- Add some fixes to the service get all API call

* Tue Jul 25 2017 Jose Castro Leon <jose.castro.leon@cern.ch> 1.4-8
- Add REST api call for configured services

* Wed May 31 2017 Jose Castro Leon <jose.castro.leon@cern.ch> 1.4.7
- Adding snapshot deletion on projects

* Thu May 18 2017 Jose Castro Leon <jose.castro.leon@cern.ch> 1.4-6
- Allow . and _ as valid characters for property names in rest interface

* Tue Jan 17 2017 Jose Castro Leon <jose.castro.leon@cern.ch> 1.4-5
- Fix bug on unsetting previous owner in a project

* Tue Jan 10 2017 Jose Castro Leon <jose.castro.leon@cern.ch> 1.4-4
- Do not detach volumes, instances with volumes will be automatically detached
- Unlock instance before delete/stop actions

* Mon Oct 03 2016 Jose Castro Leon <jose.castro.leon@cern.ch> 1.4-3
- Remove parent_id in the metadata of a project
- Unset previous owner in a project when changed

* Mon Jul 04 2016 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 1.4-2
- os-3146 Missed nova_exceptions

* Fri Jun 03 2016 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 1.4-1
- os-2913 Set global and default quota for snapshots
- os-3030 Set backup quota when project is created

* Tue May 24 2016 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 1.3-1
- os-2976 Move cornerstone from nova V1 to V2

* Fri May 13 2016 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 1.2-2
- os-2928 bugfix 'domain' is not a valid property. Changed to 'accounting-group'

* Fri May 13 2016 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 1.2-1
- os-2928 Add domain and type properties for personal projects

* Thu May 12 2016 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 1.1-7
- Change "fim-sync" to "fim-skip" to check if VMs should be shutoff when the project is disabled

* Thu Mar 31 2016 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 1.1-6
- Change "fim_lock" to "fim-lock" to check before to delete project

* Thu Nov 26 2015 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 1.1-5
- OS-2206 Check if project is already disable before project update
- OS-2202 Remove addEgroupProjectMembersGroupfilter

* Fri Nov 20 2015 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 1.1-4
- OS-2148 Improve get group in addRoleAssignment function
- OS-2149 Set keystone_timeout as float
- OS-2150 Logging improvements related with FIM
- Remove groups.list calls

* Fri Nov 06 2015 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 1.1-3
- OS-1689 Cornerstone notifications to keystone for project creation
- OS-2123 Change all DEBUG by INFO
- OS-2124 Raise proper exception when eGroup is not found in the filter

* Tue Nov 03 2015 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 1.1-2
- OS-1879-2/2077 FIX Check against ldap before send request to egroups app
- OS-2060 display_name not valid in Cinder V2
- OS-2113 Check status server before detach volume
- OS-2118 nova_timeout should be float

* Fri Oct 16 2015 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 1.1-1
- OS-1879-2 FIX Check against ldap before send request to egroups app

* Thu Oct 1 2015 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 1.1-0.el7
- OS-1593 Add REST API to cornerstone
- OS-1651 Project created without owner for non-existing user 
- OS-1658 Add REST API to cornerstoneclient
- OS-1674 Improve INFO logging mode
- OS-1675 Add confirmation delete in cornerstoneclient
- OS-1678 Add logging to cornerstoneclient
- OS-1764 Fix tests
- OS-1791 Some volume types quotas not set properly
- OS-1796 Add REST Properties call
- OS-1797 Add cornerstone-properties
- OS-1873 Check against ldap before send request to egroups app
- OS-1893 Change cinder calls to V2
- OS-1914 Add fim-lock and fim-skip by default to new shared projects
- OS-1936 Don't stop VMs with status!=active
- OS-1747 Not stop VM's in suspend/pause state
- OS-2007 project-members-set remove the project owner

* Fri Jun 12 2015 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 1.0-9.el7
- OS-1530 Add new projects to compute endpoint_group
- OS-1531 Add allow-service command
- OS-1544 Remove admin_token from settings
- OS-1545 Remove LDAP compatibility
- OS-1555 Remove duplicate add project member function
- OS-1557 Add domain param to project list
- OS-1550 Restore add user/e-group to ldap filter

* Tue Jun 02 2015 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 1.0-8.el7
- [OS-1116] Enable SQL management for projects and assigments
- [OS-1268] Enable project before delete it
- [OS-1269] Enable puppet DEBUG var for logging level
- [OS-1272] Refactor cornerstone settings
- [OS-1507] Improve project-list command
- [OS-1519] Add domain param to groups and user list calls
- [OS-1521] Add exception message to return object in all actions

* Tue Feb 24 2015 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 1.0-7.el7
- Fixed import sleep lost

* Tue Feb 24 2015 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 1.0-6.el7
- Added missed exception objects and enabled python3 compatibility

* Thu Feb 12 2015 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 1.0-5.el7
- Code improvements
- Set default domain in domain list

* Tue Jan 20 2015 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 1.0-3.el7
- Version for CentOS 7
- Replaced require library: Django14 => django
- Set quota on new volume type to 0 for all projects (OS-960)
- Tests improved
- Error messages improved

* Wed Nov 19 2014 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 1.0-2.slc6
- Added cinder library dependency

* Wed Jun 25 2014 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 1.0-1.slc6
- Code refactor
- Added new feature domain-list
- Enabled image and volume deletion on project-delete task

* Wed Mar 12 2014 Jose Castro Leon <jose.castro.leon@cern.ch> 0.9-1.slc6
- CRUD on role assignment for projects
- Improved lifecycle management for projects
- remove unused functionality

* Fri Feb 21 2014 Jose Castro Leon <jose.castro.leon@cern.ch> 0.5-2.slc6
- Include local settings overrides

* Fri Feb 21 2014 Jose Castro Leon <jose.castro.leon@cern.ch> 0.5-1.slc6
- Allow accounting and operators roles
- Upgrade keystone connections to use v3

* Fri Oct 25 2013 Jose Castro Leon <jose.castro.leon@cern.ch> 0.4-2.ai6
- Fixes for api rate limiting and conflicts on nova operations

* Wed Oct 23 2013 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 0.4-1.ai6
- FIM Policies for NOVA and GLANCE

* Mon Jul 29 2013 Jose Castro Leon <jose.castro.leon@cern.ch> 0.3-1.ai6
- Introduce ownership management in projects
- Added interaction with egroups application
- Added egroup management for different account types

* Fri Mar 15 2013 Jose Castro Leon <jose.castro.leon@cern.ch> 0.2-1.ai6
- Added more configuration parameters

* Thu Mar 07 2013 Jose Castro Leon <jose.castro.leon@cern.ch> 0.1-1.ai6
- Initial version of the tool
