# PBR should always appear first
pbr>=5.5.0 # Apache-2.0
Django>=3.2,<3.3 # BSD
djangorestframework
python-keystoneclient
python-mistralclient
pymysql
-e git+https://gitlab.cern.ch/cloud-infrastructure/pyws.git@cern#egg=pyws
zeep
