import logging

from keystoneauth1.identity import v3
from keystoneauth1 import session as keystone_session
from keystoneclient import exceptions as k_excep
from keystoneclient.v3 import client as keystone_client_v3

# Get an instance of a logger
LOGGER = logging.getLogger(__name__)
logging.getLogger("requests").setLevel(logging.WARNING)
logging.getLogger("urllib3").setLevel(logging.WARNING)


class KeystoneClient(object):
    def __init__(self, username=None, password=None, project_id=None,
                 project_name=None, auth_url=None):
        auth = v3.Password(auth_url=auth_url,
                           username=username,
                           password=password,
                           project_id=project_id,
                           project_name=project_name,
                           user_domain_id='default',
                           project_domain_id='default')

        self.session = keystone_session.Session(auth=auth)
        self.client = keystone_client_v3.Client(session=self.session)

    def get_role_by_name(self, role_name):
        """Retrieve the role object that make match with role_name."""
        LOGGER.info("Searching for role '%s'", role_name)
        roles = [r for r in self.client.roles.list()
                 if r.name == role_name]
        if roles:
            return roles[0]
        else:
            raise k_excep.NotFound(
                "Role not found using name '{0}'".format(role_name))

    def list_project(self):
        """Retrieve the list of projects."""
        return self.client.projects.list(domain='default')

    def list_role_assignments(self, user=None, group=None, project=None,
                              domain=None, role=None, effective=False):
        """Retrieve the role assignment list."""
        return self.client.role_assignments.list(user=user,
                                                 group=group,
                                                 project=project,
                                                 domain=domain,
                                                 role=role,
                                                 effective=effective)
