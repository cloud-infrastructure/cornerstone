import json
import logging

from mistralclient.api.base import APIException
from mistralclient.api import client as mistral_client
from time import sleep


# Get an instance of a logger
LOGGER = logging.getLogger(__name__)
logging.getLogger("requests").setLevel(logging.WARNING)
logging.getLogger("urllib3").setLevel(logging.WARNING)


class MistralClient(object):
    def __init__(self, session, mistral_url):
        self.client = mistral_client.client(
            session=session,
            mistral_url=mistral_url
        )

    def workflow_list(self):
        return self.client.workflows.list()

    def workflow_get(self, workflow_id):
        return self.client.workflows.get(workflow_id)

    def execution_create(self, workflow_identifier,
                         workflow_input, description):
        return self.client.executions.create(
            wf_identifier=workflow_identifier,
            workflow_input=workflow_input,
            description=description)

    def execution_list(self):
        return self.client.executions.list()

    def execution_get(self, execution_id):
        return self.client.executions.get(execution_id)

    def execution_create_sync(self, workflow_id, workflow_input, description,
                              timeout, max_executions):
        state = 'ERROR'
        output = {}
        retries = max_executions
        try:
            ex = self.execution_create(workflow_identifier=workflow_id,
                                       workflow_input=workflow_input,
                                       description=description)
            while retries > 0:
                ex = self.execution_get(ex.id)
                if ex.state != u'RUNNING':
                    break
                retries -= 1
                sleep(timeout)

            if ex.state == u'SUCCESS':
                try:
                    output = json.loads(ex.output)
                except ValueError:
                    pass
            state = ex.state
        except APIException as exception:
            LOGGER.exception(exception)
        return state, output

    def execution_create_async(self, workflow_id, workflow_input, description):
        try:
            self.execution_create(workflow_identifier=workflow_id,
                                  workflow_input=workflow_input,
                                  description=description)
        except APIException as exception:
            LOGGER.exception(exception)
