import logging
import unittest

from cornerstone import utils


class TestUtils(unittest.TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_remove_accents(self):
        self.assertEqual(
            utils.remove_accents('àáäåã'),
            'aaaaa')
