import logging
import unittest

from cornerstone.driver.mistral import MistralManager
from cornerstone import settings
from unittest import mock


class TestMistral(unittest.TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)
        settings.MISTRAL_RETRIES = 0
        settings.MISTRAL_TIMEOUT = 0
        settings.DOMAINS = ['fake_domain']

    def test_no_workflow_name(self):
        self.assertIsNone(
            MistralManager().get_workflow_name('non_existing'))

    @mock.patch('cornerstone.driver.mistral.MISTRALCLI')
    @mock.patch('cornerstone.driver.mistral.KC_ADMIN')
    def test_execute_workflow_async(self, mock_kc, mock_mc):
        mock_mc.workflow_get.return_value
        mock_mc.execution_create_sync.return_value

        MistralManager().execute_workflow('fake_n', 'fake_i', sync=False)

        mock_mc.workflow_get.assert_called_with('fake_n')
        mock_mc.execution_create_async.assert_called()

    @mock.patch('cornerstone.driver.mistral.MISTRALCLI')
    @mock.patch('cornerstone.driver.mistral.KC_ADMIN')
    def test_execute_workflow_sync(self, mock_kc, mock_mc):
        mock_mc.workflow_get.return_value
        mock_mc.execution_create_sync.return_value = 'SUCCESS', 'SUCCESS'

        MistralManager().execute_workflow('fake_n', 'fake_i')

        mock_mc.workflow_get.assert_called_with('fake_n')
        mock_mc.execution_create_sync.assert_called()

    @mock.patch('cornerstone.driver.mistral.MISTRALCLI')
    @mock.patch('cornerstone.driver.mistral.KC_ADMIN')
    def test_execute_workflow_sync_fail(self, mock_kc, mock_mc):
        mock_mc.workflow_get.return_value
        mock_mc.execution_create_sync.return_value = 'Fail', 'Fail'

        MistralManager().execute_workflow('fake_n', 'fake_i')

        mock_mc.workflow_get.assert_called_with('fake_n')
        mock_mc.execution_create_sync.assert_called()

    @mock.patch('cornerstone.driver.mistral.MISTRALCLI')
    @mock.patch('cornerstone.driver.mistral.KC_ADMIN')
    def test_execute_workflow_exception(self, mock_kc, mock_mc):
        from cornerstone.clients.mistral_client import APIException
        mock_mc.workflow_get.side_effect = APIException

        self.assertRaises(Exception,
                          MistralManager().execute_workflow(
                              'fake_n', 'fake_i'))

    @mock.patch('cornerstone.driver.mistral.MistralManager.execute_workflow')
    def test_service_enable(self, mock_ew):
        MistralManager().service_enable('fake_n', 'fake_id')
        mock_ew.assert_called_with('service_enable.fake_n', {'id': 'fake_id'})

    @mock.patch('cornerstone.driver.mistral.MistralManager.execute_workflow')
    def test_service_disable(self, mock_ew):
        MistralManager().service_disable('fake_n', 'fake_id')

        mock_ew.assert_called_with('service_disable.fake_n',
                                   {'id': 'fake_id'})

    @mock.patch('cornerstone.driver.mistral.MistralManager.execute_workflow')
    def test_add_grant_project(self, mock_ew):
        MistralManager().add_grant_project('fake_id', 'fake_r', 'fake_m')

        mock_ew.assert_called_with('project_grants.add',
                                   {'project': 'fake_id', 'members':
                                    ['fake_m'], 'role': 'fake_r'})

    @mock.patch('cornerstone.driver.mistral.MistralManager.execute_workflow')
    def test_delete_grant_project(self, mock_ew):
        MistralManager().delete_grant_project('fake_id', 'fake_r', 'fake_m')

        mock_ew.assert_called_with('project_grants.revoke',
                                   {'project': 'fake_id', 'members':
                                    ['fake_m'], 'role': 'fake_r'})

    def test_domain_list(self):
        ret = MistralManager().domain_list()

        if ret != [{'id': 'fake_domain'}]:
            assert False

    @mock.patch('cornerstone.driver.mistral.MistralManager.execute_workflow')
    def test_get_project_members(self, mock_ew):
        mock_ew.return_value = {'grants': 'fake'}
        ret = MistralManager().get_project_members('fake_id', 'fake_rol')

        mock_ew.assert_called_with('project_grants.list',
                                   {'project': 'fake_id',
                                    'role': 'fake_rol'})
        if ret != 'fake':
            assert False

    @mock.patch('cornerstone.driver.mistral.MistralManager.execute_workflow')
    def test_is_allowed(self, mock_ew):
        mock_ew.return_value = {'check': 'fake'}
        ret = MistralManager().is_allowed('fake_id', 'fake_group', 'fake_rol')

        mock_ew.assert_called_with('project_grants.check',
                                   {'project': 'fake_id',
                                    'member': 'fake_group',
                                    'role': 'fake_rol'})
        if ret != 'fake':
            assert False

    @mock.patch('cornerstone.driver.mistral.MistralManager.execute_workflow')
    def test_project_create(self, mock_ew):
        MistralManager().project_create(
            project_id='fake_id',
            name='fake_n',
            description='fake_desc',
            enabled=True,
            owner='fake_owner')

        mock_ew.assert_called_with(
            'project_create.init', {
                'id': 'fake_id',
                'name': 'fake_n',
                'description': 'fake_desc',
                'enabled': True,
                'status': '',
                'type': '',
                'owner': 'fake_owner',
                'administrator': ''})

    @mock.patch('cornerstone.driver.mistral.MistralManager.execute_workflow')
    def test_project_create_status(self, mock_ew):
        MistralManager().project_create(
            project_id='fake_id',
            name='fake_n',
            description='fake_desc',
            enabled=None,
            status='active',
            owner='fake_owner')

        mock_ew.assert_called_with(
            'project_create.init', {
                'id': 'fake_id',
                'name': 'fake_n',
                'description': 'fake_desc',
                'enabled': True,
                'status': 'active',
                'type': '',
                'owner': 'fake_owner',
                'administrator': ''})

    @mock.patch('cornerstone.driver.mistral.MistralManager.execute_workflow')
    def test_project_delete(self, mock_ew):
        MistralManager().project_delete('fake_id')

        mock_ew.assert_called_with('project_delete.init', {'id': 'fake_id'})

    @mock.patch('cornerstone.driver.mistral.MistralManager.execute_workflow')
    def test_project_update(self, mock_ew):
        MistralManager().project_update(
            project_id='fake_id',
            name='fake_n',
            description='fake_desc',
            enabled=False,
            owner='fake_owner')

        mock_ew.assert_called_with(
            'project_update.init', {
                'id': 'fake_id',
                'name': 'fake_n',
                'description': 'fake_desc',
                'enabled': False,
                'status': '',
                'type': '',
                'owner': 'fake_owner',
                'administrator': ''})

    @mock.patch('cornerstone.driver.mistral.MistralManager.execute_workflow')
    def test_project_update_status(self, mock_ew):
        MistralManager().project_update(
            project_id='fake_id',
            name='fake_n',
            description='fake_desc',
            enabled=None,
            status='blocked',
            owner='fake_owner')

        mock_ew.assert_called_with(
            'project_update.init', {
                'id': 'fake_id',
                'name': 'fake_n',
                'description': 'fake_desc',
                'enabled': False,
                'status': 'blocked',
                'type': '',
                'owner': 'fake_owner',
                'administrator': ''})

    @mock.patch('cornerstone.driver.mistral.MistralManager.execute_workflow')
    def test_project_get(self, mock_ew):
        MistralManager().project_get('fake_id')

        mock_ew.assert_called_with('project_get.init', {'id': 'fake_id'})

    @mock.patch('cornerstone.driver.mistral.MistralManager.execute_workflow')
    def test_project_getinfo(self, mock_ew):
        MistralManager().project_getinfo('fake_id')

        mock_ew.assert_called_with('project_get.detailed', {'id': 'fake_id'})

    @mock.patch('cornerstone.driver.mistral.KC_ADMIN')
    def test_project_list(self, mock_kc):
        mock_kc.list_project.return_value = [
            mock.Mock(id='fake_id', owner='f'),
            mock.Mock(id='fake_id2', owner='f')]
        mock_kc.list_role_assignments.side_effect = iter([[
            mock.Mock(scope={'project': {'id': 'fake_id'}},
                      user={'id': 'fake_id'}),
            mock.Mock(scope={'project': {'id': 'fake_id2'}},
                      user={'id': 'fake_id2'}),
            mock.Mock(scope={'project2': {'id': 'fake_id2'}},
                      user={'id': 'fake_id2'})], [
            mock.Mock(scope={'project': {'id': 'fake_id'}},
                      group={'id': 'fake_id'}),
            mock.Mock(scope={'project': {'id': 'fake_id2'}},
                      group={'id': 'fake_id2'}),
            mock.Mock(scope={'project2': {'id': 'fake_id2'}},
                      group={'id': 'fake_id2'})]])
        MistralManager().project_list()

        mock_kc.list_project.assert_called()
        mock_kc.get_role_by_name.assert_called()
        mock_kc.list_role_assignments.assert_called()

    @mock.patch('cornerstone.driver.mistral.MistralManager.is_allowed')
    def test_project_list_groups_grants(self, mock_ia):
        mock_ia.return_value = True
        MistralManager().project_list_groups_grants('fake_id')

        mock_ia.assert_called()

    @mock.patch('cornerstone.driver.mistral.MistralManager.is_allowed')
    def test_project_list_groups_grants_fail(self, mock_ia):
        mock_ia.return_value = False
        MistralManager().project_list_groups_grants('fake_id')

        mock_ia.assert_called()

    @mock.patch('cornerstone.driver.mistral.MistralManager.execute_workflow')
    def test_project_add_property(self, mock_ew):
        MistralManager().project_add_property(
            'fake_id', 'fake_p_name', 'fake_p_val')

        mock_ew.assert_called_with('project_property.update', {
            'project': 'fake_id',
            'properties': {'fake_p_name': 'fake_p_val'}})

    @mock.patch('cornerstone.driver.mistral.MistralManager.execute_workflow')
    def test_project_delete_property(self, mock_ew):
        MistralManager().project_delete_property('fake_id', 'fake_p_name')

        mock_ew.assert_called_with('project_property.update', {
            'project': 'fake_id',
            'properties': {'fake_p_name': ''}})

    @mock.patch('cornerstone.driver.mistral.MistralManager.execute_workflow')
    def test_project_list_property(self, mock_ew):
        mock_ew.return_value = {'properties': 'fake_p'}
        ret = MistralManager().project_list_property('fake_id')

        mock_ew.assert_called_with('project_property.list',
                                   {'project': 'fake_id'})
        if ret != 'fake_p':
            assert False

    @mock.patch('cornerstone.driver.mistral.MistralManager.execute_workflow')
    def test_project_update_property(self, mock_ew):
        MistralManager().project_update_property('fake_id',
                                                 'fake_p_name',
                                                 'fake_p_value')

        mock_ew.assert_called_with('project_property.update',
                                   {'project': 'fake_id',
                                    'properties':
                                    {'fake_p_name': 'fake_p_value'}})

    @mock.patch('cornerstone.driver.mistral.MistralManager.execute_workflow')
    def test_service_list(self, mock_ew):
        mock_ew.return_value = {'services': 'fake_s'}
        ret = MistralManager().service_list()

        mock_ew.assert_called_with('service_list.init', {})
        if ret != 'fake_s':
            assert False

    @mock.patch('cornerstone.driver.mistral.MistralManager.execute_workflow')
    def test_project_service_list(self, mock_ew):
        mock_ew.return_value = {'services': 'fake_s'}
        ret = MistralManager().project_service_list('fake_id')

        mock_ew.assert_called_with('service_list.project',
                                   {'project': 'fake_id'})
        if ret != 'fake_s':
            assert False

    @mock.patch('cornerstone.driver.mistral.MistralManager.execute_workflow')
    def test_grant_group_project(self, mock_ew):
        MistralManager().grant_group_project(
            'fake_id', 'fake_g_name', 'fake_r_name')
        mock_ew.assert_called_with('project_grants.add',
                                   {'project': 'fake_id',
                                    'members': ['fake_g_name'],
                                    'role': 'fake_r_name'})

    @mock.patch('cornerstone.driver.mistral.MistralManager.execute_workflow')
    def test_revoke_group_project(self, mock_ew):
        MistralManager().revoke_group_project(
            'fake_id', 'fake_g_name', 'fake_r_name')
        mock_ew.assert_called_with('project_grants.revoke',
                                   {'project': 'fake_id',
                                    'members': ['fake_g_name'],
                                    'role': 'fake_r_name'})

    @mock.patch('cornerstone.driver.mistral.MistralManager.execute_workflow')
    def test_set_project_members(self, mock_ew):
        MistralManager().set_project_members('fake_id', 'fake_r_name',
                                             'fake_member')

        mock_ew.assert_called_with('project_grants.set',
                                   {'project':
                                    'fake_id',
                                    'members': ['fake_member'],
                                    'role': 'fake_r_name'})
