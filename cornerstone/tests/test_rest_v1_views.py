import logging
import unittest

from cornerstone.rest.v1 import views
from django.core.exceptions import ObjectDoesNotExist
from unittest import mock


class TestRestV1Views(unittest.TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    @mock.patch('cornerstone.rest.v1.views.AssignmentSerializer')
    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_get(self, mock_re, mock_cs, mock_as):
        mock_as.return_value = mock.Mock(data='fake_d')
        mock_re.return_value = 'fake_response'
        mock_cs.get_project_members.return_value = 'fake_m'
        ret = views.AssignmentList().get('fake_req', 'fake_id', 'fake_rol')

        mock_cs.get_project_members.assert_called_with('fake_id', 'fake_rol')
        mock_as.assert_called_with('fake_m', many=True)
        if ret != 'fake_response':
            assert False

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_get_f(self, mock_re, mock_cs):
        mock_re.return_value = 'fake_response'
        mock_cs.get_project_members.return_value = False
        ret = views.AssignmentList().get('fake_req', 'fake_id', 'fake_rol')

        mock_cs.get_project_members.assert_called_with('fake_id', 'fake_rol')
        if ret != 'fake_response':
            assert False

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_get_f_excep(self, mock_re, mock_cs):
        mock_re.return_value = 'fake_response'
        mock_cs.get_project_members.side_effect = Exception('fake_excep')

        self.assertRaises(
            Exception, views.AssignmentList().get(
                'fake_req', 'fake_id', 'fake_rol'))
        mock_re.assert_called_with(
            {'detail': 'fake_excep'}, status=400)

    @mock.patch('cornerstone.rest.v1.views.Response')
    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.AssignmentInputSerializer')
    def test_post(self, mock_ai, mock_cs, mock_re):
        mock_ai.return_value = mock.Mock(
            data={'members': [{'name': 'f1'}, {'name': 'f2'}]})
        mock_cs.add_grant_project.return_value = 'fake'
        mock_re.return_value = 'fake_response'
        views.AssignmentList().post(mock.Mock(
            data='fake_r'), 'fake_id', 'fake_rol')

        mock_ai.assert_called_with(data='fake_r')
        mock_cs.add_grant_project.assert_called_with(
            'fake_id', 'fake_rol', 'f1,f2')
        mock_re.assert_called_with(
            {'members': [{'name': 'f1'}, {'name': 'f2'}]}, status=201)

    @mock.patch('cornerstone.rest.v1.views.Response')
    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.AssignmentInputSerializer')
    def test_post_2(self, mock_ai, mock_cs, mock_re):
        mock_ai.return_value = mock.Mock(data={'members': {'name': 'f1'}})
        mock_cs.add_grant_project.return_value = 'fake'
        mock_re.return_value = 'fake_response'
        views.AssignmentList().post(
            mock.Mock(data='fake_r'), 'fake_id', 'fake_rol')

        mock_ai.assert_called_with(data='fake_r')
        mock_cs.add_grant_project.assert_called_with(
            'fake_id', 'fake_rol', 'f1')
        mock_re.assert_called_with(
            {'members': {'name': 'f1'}}, status=201)

    @mock.patch('cornerstone.rest.v1.views.Response')
    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.AssignmentInputSerializer')
    def test_post_excep(self, mock_ai, mock_cs, mock_re):
        mock_ai.return_value = mock.Mock(data={'members': []})
        mock_cs.add_grant_project.side_effect = Exception('fake_excep')
        mock_re.return_value = 'fake_response'

        self.assertRaises(
            Exception, views.AssignmentList().post(
                mock.Mock(data='fake_r'), 'fake_id', 'fake_rol'))
        mock_re.assert_called_with(
            {'detail': 'Members not found on request'}, status=400)

    @mock.patch('cornerstone.rest.v1.views.Response')
    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.AssignmentInputSerializer')
    def test_post_fail1(self, mock_ai, mock_cs, mock_re):
        mock_ai.return_value = mock.Mock(data={'members': {'name': 'f1'}})
        mock_cs.add_grant_project.return_value = ''
        mock_re.return_value = 'fake_response'
        views.AssignmentList().post(
            mock.Mock(data='fake_r'), 'fake_id', 'fake_rol')

        mock_ai.assert_called_with(data='fake_r')
        mock_cs.add_grant_project.assert_called_with(
            'fake_id', 'fake_rol', 'f1')
        mock_re.assert_called_with(
            {'detail': 'There was an error adding an assignment'}, status=412)

    @mock.patch('cornerstone.rest.v1.views.Response')
    @mock.patch('cornerstone.rest.v1.views.AssignmentInputSerializer')
    def test_post_serialicer_no(self, mock_ai, mock_re):
        m = mock.MagicMock(errors='fake_error')
        m.is_valid.return_value = False
        mock_ai.return_value = m
        views.AssignmentList().post(
            mock.Mock(data='fake_r'), 'fake_id', 'fake_rol')

        mock_ai.assert_called_with(data='fake_r')
        mock_re.assert_called_with('fake_error', status=400)

    @mock.patch('cornerstone.rest.v1.views.AssignmentInputSerializer')
    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    def test_put(self, mock_cs, mock_ai):
        mock_ai.return_value = mock.Mock(
            data={'members': [{'name': 'f1'}, {'name': 'f2'}]})
        mock_cs.set_project_members.return_value = 'fake_ret'
        views.AssignmentList().put(
            mock.Mock(data='fake_r'), 'fake_id', 'fake_rol')

    @mock.patch('cornerstone.rest.v1.views.Response')
    @mock.patch('cornerstone.rest.v1.views.AssignmentInputSerializer')
    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    def test_put_no_ret(self, mock_cs, mock_ai, mock_re):
        mock_re.return_value = 'fake_ret'
        mock_ai.return_value = mock.Mock(
            data={'members': [{'name': 'f1'}, {'name': 'f2'}]})
        mock_cs.set_project_members.return_value = ''
        views.AssignmentList().put(
            mock.Mock(data='fake_r'), 'fake_id', 'fake_rol')

        mock_ai.assert_called_with(data='fake_r')
        mock_cs.set_project_members.assert_called_with(
            'fake_id', 'fake_rol', 'f1,f2')
        mock_re.assert_called_with(
            {'detail': 'There was an error setting an assignment'},
            status=412)

    @mock.patch('cornerstone.rest.v1.views.Response')
    @mock.patch('cornerstone.rest.v1.views.AssignmentInputSerializer')
    def test_put_no_valid(self, mock_ai, mock_re):
        m = mock.MagicMock(errors='fake_error')
        m.is_valid.return_value = False
        mock_ai.return_value = m
        mock_re.return_value = 'fake_res'
        views.AssignmentList().put(
            mock.Mock(data='fake_r'), 'fake_id', 'fake_rol')

        mock_ai.assert_called_with(data='fake_r')
        mock_re.assert_called_with('fake_error', status=400)

    @mock.patch('cornerstone.rest.v1.views.Response')
    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.AssignmentInputSerializer')
    def test_put_excep(self, mock_ai, mock_cs, mock_re):
        mock_ai.return_value = mock.Mock(data={'members': []})
        mock_re.return_value = 'fake_response'
        mock_cs.set_project_members.side_effect = Exception('fake_excep')

        self.assertRaises(
            Exception, views.AssignmentList().put(
                mock.Mock(data='fake_r'), 'fake_id', 'fake_rol'))
        mock_re.assert_called_with(
            {'detail': 'Members not found on request'}, status=400)

    @mock.patch('cornerstone.rest.v1.views.Response')
    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.AssignmentInputSerializer')
    def test_delete(self, mock_ai, mock_cs, mock_re):
        mock_ai.return_value = 'fake_r'
        mock_cs.delete_grant_project.return_value = 'fake_ret'
        mock_re.return_value = 'fake_response'
        views.AssignmentList().delete(
            mock.Mock(data='fake_r'), 'fake_id',
            'fake_rol', membername='fake_m')

        mock_ai.assert_called_with(data='fake_r')
        mock_cs.delete_grant_project.assert_called_with(
            'fake_id', 'fake_rol', 'fake_m')
        mock_re.assert_called_with(status=204)

    @mock.patch('cornerstone.rest.v1.views.Response')
    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.AssignmentInputSerializer')
    def test_delete_memberNone(self, mock_ai, mock_cs, mock_re):
        mock_ai.return_value = 'fake_r'
        mock_cs.delete_grant_project.return_value = 'fake_ret'
        mock_re.return_value = 'fake_response'
        views.AssignmentList().delete(
            mock.Mock(data={'members': [{'name': 'f1'}, {'name': 'f2'}]}),
            'fake_id', 'fake_rol')

        mock_ai.assert_called_with(
            data={'members': [{'name': 'f1'}, {'name': 'f2'}]})
        mock_cs.delete_grant_project.assert_called_with(
            'fake_id', 'fake_rol', 'f1,f2')
        mock_re.assert_called_with(status=204)

    @mock.patch('cornerstone.rest.v1.views.Response')
    @mock.patch('cornerstone.rest.v1.views.AssignmentInputSerializer')
    def test_delete_no_project(self, mock_ai, mock_re):
        mock_ai.return_value = mock.Mock(errors='fake_errors')
        mock_re.return_value = 'fake_response'
        views.AssignmentList().delete(
            mock.Mock(data='fake_r'), project_id='',
            rolename='fake_rol', membername='fake_m')

        mock_ai.assert_called_with(data='fake_r')
        mock_re.assert_called_with('fake_errors', status=400)

    @mock.patch('cornerstone.rest.v1.views.Response')
    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.AssignmentInputSerializer')
    def test_delete_no_ret(self, mock_ai, mock_cs, mock_re):
        mock_ai.return_value = 'fake_r'
        mock_cs.delete_grant_project.return_value = ''
        mock_re.return_value = 'fake_response'
        views.AssignmentList().delete(
            mock.Mock(data='fake_r'), 'fake_id',
            'fake_rol', membername='fake_m')

        mock_ai.assert_called_with(data='fake_r')
        mock_cs.delete_grant_project.assert_called_with(
            'fake_id', 'fake_rol', 'fake_m')
        mock_re.assert_called_with(
            {'detail': 'There was an error removing an assignment'},
            status=412)

    @mock.patch('cornerstone.rest.v1.views.Response')
    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.AssignmentInputSerializer')
    def test_delete_excep(self, mock_ai, mock_cs, mock_re):
        mock_ai.return_value = 'fake_r'
        mock_cs.delete_grant_project.side_effect = Exception('fake_excep')
        mock_re.return_value = 'fake_response'
        self.assertRaises(
            Exception, views.AssignmentList().delete(
                mock.Mock(data='fake_r'), 'fake_id',
                'fake_rol', membername='fake_m'))

        mock_re.assert_called_with(
            {'detail': 'fake_excep'}, status=400)

    @mock.patch('cornerstone.rest.v1.views.Response')
    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.DomainSerializer')
    def test_get_Domain(self, mock_ds, mock_cs, mock_re):
        mock_cs.domain_list.return_value = 'fake_domain'
        mock_ds.return_value = mock.Mock(data='fake_data')
        views.DomainList().get('fake_r')

        mock_ds.assert_called_with('fake_domain', many=True)
        mock_cs.domain_list.assert_called()
        mock_re.assert_called_with('fake_data')

    @mock.patch('cornerstone.rest.v1.views.Response')
    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.DomainSerializer')
    def test_get_Domain_excep(self, mock_ds, mock_cs, mock_re):
        mock_cs.domain_list.side_effect = IOError(
            -1, 'f_stderror', 'f_filename')
        views.DomainList().get('fake_r')

        mock_re.assert_called_with(
            {'detail': 'f_stderror: f_filename'}, status=400)

    @mock.patch('cornerstone.rest.v1.views.Response')
    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.GrantSerializer')
    def test_get_grantlist(self, mock_gs, mock_cs, mock_re):
        mock_gs.return_value = mock.Mock(data='fake_data')
        mock_cs.project_list_groups_grants.return_value = 'fake_list'
        mock_re.return_value('fake_re')
        views.GrantList().get('fake_r', 'fake_id')

        mock_cs.project_list_groups_grants.assert_called_with('fake_id')
        mock_gs.assert_called_with('fake_list', many=True)
        mock_re.assert_called_with('fake_data')

    @mock.patch('cornerstone.rest.v1.views.GrantList.postdelete')
    def test_delete_grantlist(self, mock_pd):
        m = mock.Mock(data={'group_name': 'fake_gn'})
        views.GrantList().delete(
            m, 'fake_id')

        mock_pd.assert_called_with(False, 'fake_id', 'fake_gn', m, None)

    @mock.patch('cornerstone.rest.v1.views.GrantList.postdelete')
    def test_delete_grantlist_group_name(self, mock_pd):
        views.GrantList().delete(
            'fake_req', 'fake_id', groupname='fake_gn')

        mock_pd.assert_called_with(
            False, 'fake_id', 'fake_gn', 'fake_req', None)

    @mock.patch('cornerstone.rest.v1.views.GrantList.postdelete')
    def test_post_grantlist_gn(self, mock_pd):
        m = mock.Mock(data={'group_name': 'fake_gn'})
        views.GrantList().post(m, 'fake_id')

        mock_pd.assert_called_with(True, 'fake_id', 'fake_gn', m, None)

    @mock.patch('cornerstone.rest.v1.views.GrantList.postdelete')
    def test_post_grantlist(self, mock_pd):
        m = mock.Mock(data='fake')
        views.GrantList().post(m, 'fake_id')

        mock_pd.assert_called_with(True, 'fake_id', None, m, None)

    @mock.patch('cornerstone.rest.v1.views.GrantList.gr_rel')
    @mock.patch('cornerstone.rest.v1.views.Response')
    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.GrantSerializer')
    def test_postdelete_grantlist_enabled(self, mock_gs, mock_cs,
                                          mock_re, mock_gr):
        m = mock.MagicMock(data='fake_d')
        m.is_valid.return_value = True
        mock_gs.return_value = m
        views.GrantList().postdelete(True, 'fake_id', 'fake_gn', 'fake_r')

        mock_re.assert_called_with('fake_d', status=201)

    @mock.patch('cornerstone.rest.v1.views.GrantList.gr_rel')
    @mock.patch('cornerstone.rest.v1.views.Response')
    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.GrantSerializer')
    def test_postdelete_grantlist(self, mock_gs, mock_cs, mock_re, mock_gr):
        m = mock.MagicMock(data='fake_d')
        m.is_valid.return_value = True
        mock_gs.return_value = m
        views.GrantList().postdelete(False, 'fake_id', 'fake_gn', 'fake_r')

        mock_re.assert_called_with('fake_d', status=204)

    @mock.patch('cornerstone.rest.v1.views.GrantList.gr_rel')
    @mock.patch('cornerstone.rest.v1.views.Response')
    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.GrantSerializer')
    def test_postdelete_grantlist_no_ret(self, mock_gs, mock_cs,
                                         mock_re, mock_gr):
        m = mock.MagicMock()
        m.is_valid.return_value = True
        mock_gs.return_value = m
        mock_cs.set_allow_group.return_value = None
        views.GrantList().postdelete(True, 'fake_id', 'fake_gn', 'fake_r')

        mock_re.assert_called_with(
            {'detail': 'There was an error adding/deleting a grant'},
            status=412)

    @mock.patch('cornerstone.rest.v1.views.Response')
    @mock.patch('cornerstone.rest.v1.views.GrantSerializer')
    def test_postdelete_grantlist_novalid(self, mock_gs, mock_re):
        m = mock.MagicMock(errors='fake_data')
        m.is_valid.return_value = False
        mock_gs.return_value = m
        views.GrantList().postdelete(True, 'fake_id', 'fake_gn', 'fake_r')

        mock_re.assert_called_with('fake_data', status=400)

    @mock.patch('cornerstone.rest.v1.views.GrantList.gr_rel')
    @mock.patch('cornerstone.rest.v1.views.Response')
    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.GrantSerializer')
    def test_postdelete_grantlist_exception(
            self, mock_gs, mock_cs, mock_re, mock_gr):
        m = mock.MagicMock(errors='fake_data')
        m.is_valid.return_value = True
        mock_gs.return_value = m
        mock_cs.set_allow_group.side_effect = Exception('fake_excep')
        mock_re.return_value = 'fake_response'

        self.assertRaises(
            Exception,
            views.GrantList().postdelete('True', 'f_id', 'f_gn', 'fr'))
        mock_re.assert_called_with({'detail': 'fake_excep'}, status=500)

    @mock.patch('cornerstone.rest.v1.views.Person')
    @mock.patch('cornerstone.rest.v1.views.PersonSerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_getPersonList(self, mock_re, mock_ps, mock_pe):
        mock_ps.return_value = mock.Mock(data='fake_data')
        views.PersonList().get('fake_req', 'fake_pk')

        mock_re.assert_called_with('fake_data')

    @mock.patch('cornerstone.rest.v1.views.PersonSerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_postPersonList(self, mock_re, mock_ps):
        m = mock.Mock(data='fake_data', errors='fake_error')
        m.is_valid.return_value = True
        mock_ps.return_value = m
        views.PersonList().post(m)

        mock_re.assert_called_with('fake_data', status=201)

    @mock.patch('cornerstone.rest.v1.views.PersonSerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_postPersonList_fail(self, mock_re, mock_ps):
        m = mock.Mock(data='fake_data', errors='fake_error')
        m.is_valid.return_value = False
        mock_ps.return_value = m
        views.PersonList().post(m)

        mock_re.assert_called_with('fake_error', status=400)

    @mock.patch('cornerstone.rest.v1.views.Person')
    def test_get_object(self, mock_pe):
        mock_pe.objects.get.return_value = 'fake'
        views.PersonDetail().get_object('fake_pk')

        mock_pe.objects.get.assert_called_with(pk='fake_pk')

    @mock.patch('cornerstone.rest.v1.views.Http404')
    @mock.patch('cornerstone.rest.v1.views.Person')
    def test_get_object_ex(self, mock_pe, mock_ht):
        mock_pe.objects.get.side_effect = ObjectDoesNotExist
        mock_ht.return_value = 'fake_error'

        with self.assertRaises(Exception):
            views.PersonDetail().get_object('fake_pk')

    @mock.patch('cornerstone.rest.v1.views.PersonDetail.get_object')
    @mock.patch('cornerstone.rest.v1.views.PersonSerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_getPersonDetail(self, mock_re, mock_ps, mock_go):
        mock_ps.return_value = mock.Mock(data='fake_data')
        views.PersonDetail().get('fake_req', 'fake_pk')

        mock_re.assert_called_with('fake_data')

    @mock.patch('cornerstone.rest.v1.views.PersonDetail.get_object')
    @mock.patch('cornerstone.rest.v1.views.PersonSerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_putPersonDetail(self, mock_re, mock_ps, mock_go):
        m = mock.Mock(data='fake_data')
        m.is_valid.return_value = True
        mock_ps.return_value = m
        views.PersonDetail().put(m, 'fake_pk')

        mock_re.assert_called_with('fake_data')

    @mock.patch('cornerstone.rest.v1.views.PersonDetail.get_object')
    @mock.patch('cornerstone.rest.v1.views.PersonSerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_putPersonDetail_fail(self, mock_re, mock_ps, mock_go):
        m = mock.Mock(errors='fake_errors', data='fake_data')
        m.is_valid.return_value = False
        mock_ps.return_value = m
        views.PersonDetail().put(m, 'fake_pk')

        mock_re.assert_called_with('fake_errors', status=400)

    @mock.patch('cornerstone.rest.v1.views.PersonDetail.get_object')
    @mock.patch('cornerstone.rest.v1.views.Person')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_deletePersonDetail(self, mock_re, mock_pe, mock_go):
        views.PersonDetail().delete('fake_r', 'fake_pk')

        mock_re.assert_called_with(status=204)

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.PropertySerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_getPropertieslist(self, mock_re, mock_ps, mock_cs):
        mock_cs.project_list_property.return_value = 'fake_prop'
        mock_ps.return_value = mock.Mock(data='fake_data')
        views.PropertiesList().get('fake_req', 'fake_pk')

        mock_cs.project_list_property.assert_called_with('fake_pk')
        mock_ps.assert_called_with('fake_prop', many=True)
        mock_re.assert_called_with('fake_data')

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.PropertySerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_postPropertieslist(self, mock_re, mock_ps, mock_cs):
        m = mock.Mock(data={'property_name': 'fake_p',
                            'property_value': 'fake_v'})
        m.is_valid.return_value = True
        mock_ps.return_value = m
        mock_cs.project_add_property.return_value = 'fake_ret'
        views.PropertiesList().post(m, 'fake_pk')

        mock_cs.project_add_property.assert_called_with('fake_pk',
                                                        'fake_p',
                                                        'fake_v')
        mock_ps.assert_called_with(data={'property_name': 'fake_p',
                                         'property_value': 'fake_v'})
        mock_re.assert_called_with({'property_name': 'fake_p',
                                    'property_value': 'fake_v'}, status=201)

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.PropertySerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_postPropertieslist_no_ret(self, mock_re, mock_ps, mock_cs):
        m = mock.Mock(data={'property_name': 'fake_p',
                            'property_value': 'fake_v'})
        m.is_valid.return_value = True
        mock_ps.return_value = m
        mock_cs.project_add_property.return_value = None
        views.PropertiesList().post(m, 'fake_pk')

        mock_cs.project_add_property.assert_called_with('fake_pk',
                                                        'fake_p',
                                                        'fake_v')
        mock_ps.assert_called_with(data={'property_name': 'fake_p',
                                         'property_value': 'fake_v'})
        mock_re.assert_called_with(
            {'detail': 'There was an error adding a property'}, status=412)

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.PropertySerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_postPropertieslist_except(self, mock_re, mock_ps, mock_cs):
        m = mock.Mock(data={'property_name': 'fake_p',
                            'property_value': 'fake_v'})
        m.is_valid.return_value = True
        mock_ps.return_value = m
        mock_cs.project_add_property.side_effect = ValueError('fake_excep')
        mock_re.return_value = 'fake_response'

        self.assertRaises(
            Exception, views.PropertiesList().post(m, 'fake_pk'))

        mock_cs.project_add_property.assert_called_with('fake_pk',
                                                        'fake_p',
                                                        'fake_v')
        mock_ps.assert_called_with(data={'property_name': 'fake_p',
                                         'property_value': 'fake_v'})
        mock_re.assert_called_with(
            {'detail': 'fake_excep'}, status=400)

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.PropertySerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_postPropertieslist_except2(self, mock_re, mock_ps, mock_cs):
        m = mock.Mock(data={'property_name': 'fake_p',
                            'property_value': 'fake_v'})
        m.is_valid.return_value = True
        mock_ps.return_value = m
        mock_cs.project_add_property.side_effect = Exception('fake_excep')
        mock_re.return_value = 'fake_response'

        self.assertRaises(
            Exception, views.PropertiesList().post(m, 'fake_pk'))

        mock_cs.project_add_property.assert_called_with('fake_pk',
                                                        'fake_p',
                                                        'fake_v')
        mock_ps.assert_called_with(data={'property_name': 'fake_p',
                                         'property_value': 'fake_v'})
        mock_re.assert_called_with(
            {'detail': 'fake_excep'}, status=500)

    @mock.patch('cornerstone.rest.v1.views.PropertySerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_postPropertlist_except_no_valid(self, mock_re, mock_ps):
        m = mock.Mock(data={'property_name': 'fake_p',
                            'property_value': 'fake_v'},
                      errors='fake_errors')
        m.is_valid.return_value = False
        mock_ps.return_value = m
        mock_re.return_value = 'fake_response'

        self.assertRaises(
            Exception, views.PropertiesList().post(m, 'fake_pk'))

        mock_ps.assert_called_with(data={'property_name': 'fake_p',
                                         'property_value': 'fake_v'})
        mock_re.assert_called_with(
            'fake_errors', status=400)

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.PropertySerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_postPropertiesDetail(self, mock_re, mock_ps, mock_cs):
        m = mock.Mock(data={'property_name': 'fake_p',
                            'property_value': 'fake_v'})
        m.is_valid.return_value = True
        mock_ps.return_value = m
        mock_cs.project_update_property.return_value = 'fake_ret'
        views.PropertiesDetail().put(m, 'fake_pk', 'fake_p_name')

        mock_cs.project_update_property.assert_called_with('fake_pk',
                                                           'fake_p',
                                                           'fake_v')
        mock_ps.assert_called_with(data={'property_name': 'fake_p',
                                         'property_value': 'fake_v'})
        mock_re.assert_called_with({'property_name': 'fake_p',
                                    'property_value': 'fake_v'})

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.PropertySerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_postPropertiesDetail_no_ret(self, mock_re, mock_ps, mock_cs):
        m = mock.Mock(data={'property_name': 'fake_p',
                            'property_value': 'fake_v'})
        m.is_valid.return_value = True
        mock_ps.return_value = m
        mock_cs.project_update_property.return_value = None
        views.PropertiesDetail().put(m, 'fake_pk', 'fake_p_name')

        mock_cs.project_update_property.assert_called_with('fake_pk',
                                                           'fake_p',
                                                           'fake_v')
        mock_ps.assert_called_with(data={'property_name': 'fake_p',
                                         'property_value': 'fake_v'})
        mock_re.assert_called_with(
            {'detail': 'There was an error updating a property'}, status=412)

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.PropertySerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_postPropertiesDetail_except(self, mock_re, mock_ps, mock_cs):
        m = mock.Mock(data={'property_name': 'fake_p',
                            'property_value': 'fake_v'})
        m.is_valid.return_value = True
        mock_ps.return_value = m
        mock_cs.project_update_property.side_effect = Exception('fake_excep')
        mock_re.return_value = 'fake_response'

        self.assertRaises(
            Exception, views.PropertiesDetail().put(
                m, 'fake_pk', 'fake_p_name'))

        mock_cs.project_update_property.assert_called_with('fake_pk',
                                                           'fake_p',
                                                           'fake_v')
        mock_ps.assert_called_with(data={'property_name': 'fake_p',
                                         'property_value': 'fake_v'})
        mock_re.assert_called_with(
            {'detail': 'fake_excep'}, status=500)

    @mock.patch('cornerstone.rest.v1.views.PropertySerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_postPropertiesDetail_novalid(self, mock_re, mock_ps):
        m = mock.Mock(data={'property_name': 'fake_p',
                            'property_value': 'fake_v'},
                      errors='fake_errors')
        m.is_valid.return_value = False
        mock_ps.return_value = m
        mock_re.return_value = 'fake_response'

        self.assertRaises(
            Exception, views.PropertiesDetail().put(
                m, 'fake_pk', 'fake_p_name'))

        mock_ps.assert_called_with(data={'property_name': 'fake_p',
                                         'property_value': 'fake_v'})
        mock_re.assert_called_with(
            'fake_errors', status=400)

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_deletePropertiesDetail(self, mock_re, mock_cs):
        views.PropertiesDetail().delete('fake_req', 'fake_pk', 'fake_p_name')

        mock_cs.project_delete_property.assert_called_with(
            'fake_pk', 'fake_p_name')
        mock_re.assert_called_with(status=204)

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_deletePropertiesDetail_no_ret(self, mock_re, mock_cs):
        mock_cs.project_delete_property.return_value = None
        views.PropertiesDetail().delete('fake_req', 'fake_pk', 'fake_p_name')

        mock_cs.project_delete_property.assert_called_with(
            'fake_pk', 'fake_p_name')
        mock_re.assert_called_with(
            {'detail': 'There was an error deleting a property'}, status=412)

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_deletePropertiesDetail_excep(self, mock_re, mock_cs):
        mock_cs.project_delete_property.side_effect = Exception('fake_excep')
        mock_re.return_value = 'fake'

        self.assertRaises(
            Exception, views.PropertiesDetail().delete(
                'fake_req', 'fake_pk', 'fake_p_name'))

        mock_cs.project_delete_property.assert_called_with(
            'fake_pk', 'fake_p_name')
        mock_re.assert_called_with(
            {'detail': 'fake_excep'}, status=500)

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.ProjectListSerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_getProjectList(self, mock_re, mock_ps, mock_cs):
        mock_cs.project_list.return_value = 'fake_projects'
        mock_ps.return_value = mock.Mock(data='fake_data')
        views.ProjectList().get('fake_req')

        mock_cs.project_list.assert_called()
        mock_ps.assert_called_with('fake_projects', many=True)
        mock_re.assert_called_with('fake_data')

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.ProjectSerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_postProjectList(self, mock_re, mock_ps, mock_cs):
        m = mock.Mock(data={'id': 'fake_id',
                            'name': 'fake_name',
                            'description': 'fake_desc',
                            'enabled': 'fake_enable',
                            'owner': 'fake_owner'})
        m.is_valid.return_value = True
        mock_ps.return_value = m
        views.ProjectList().post(mock.Mock(data='fake_req'))

        mock_ps.assert_called_with(data='fake_req')
        mock_cs.project_create.assert_called_with(
            project_id='fake_id',
            name='fake_name',
            description='fake_desc',
            enabled='fake_enable',
            owner='fake_owner')
        mock_re.assert_called_with(
            {'id': 'fake_id',
             'name': 'fake_name',
             'description': 'fake_desc',
             'enabled': 'fake_enable',
             'owner': 'fake_owner'}, status=201)

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.ProjectSerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_postProjectList_no_project(self, mock_re, mock_ps, mock_cs):
        m = mock.Mock(data={'id': 'fake_id',
                            'name': 'fake_name',
                            'description': 'fake_desc',
                            'enabled': 'fake_enable',
                            'owner': 'fake_owner'})
        m.is_valid.return_value = True
        mock_ps.return_value = m
        mock_cs.project_create.return_value = None
        views.ProjectList().post(mock.Mock(data='fake_req'))

        mock_ps.assert_called_with(data='fake_req')
        mock_re.assert_called_with(
            {'detail': 'Project create failed on "fake_id"'}, status=412)

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.ProjectSerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_postProjectList_except1(self, mock_re, mock_ps, mock_cs):
        m = mock.MagicMock()
        m.is_valid.return_value = True
        mock_ps.return_value = m
        mock_cs.project_create.side_effect = Exception('fail_project')
        views.ProjectList().post(mock.Mock(data='fake_req'))

        mock_ps.assert_called_with(data='fake_req')
        mock_re.assert_called_with(
            {'detail': 'fail_project'}, status=500)

    @mock.patch('cornerstone.rest.v1.views.ProjectSerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_postProjectList_except2(self, mock_re, mock_ps):
        m = mock.Mock(errors='fake_errors')
        m.is_valid.return_value = False
        mock_ps.return_value = m
        views.ProjectList().post(mock.Mock(data='fake_req'))

        mock_ps.assert_called_with(data='fake_req')
        mock_re.assert_called_with('fake_errors', status=400)

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.ProjectSerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_getProjectDetail(self, mock_re, mock_ps, mock_cs):
        mock_cs.project_getinfo.return_value = 'fake_project'
        mock_ps.return_value = mock.Mock(data='fake_data')
        views.ProjectDetail().get('fake_req', 'fake_pk')

        mock_cs.project_getinfo.assert_called_with('fake_pk')
        mock_ps.assert_called_with('fake_project')
        mock_re.assert_called_with('fake_data')

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_getProjectDetail_no_project(self, mock_re, mock_cs):
        mock_cs.project_getinfo.return_value = None
        views.ProjectDetail().get('fake_req', 'fake_pk')

        mock_cs.project_getinfo.assert_called_with('fake_pk')
        mock_re.assert_called_with(
            {'detail': 'Project get failed on "fake_pk"'}, status=412)

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_getProjectDetail_exception(self, mock_re, mock_cs):
        mock_cs.project_getinfo.side_effect = Exception('fake_exception')
        views.ProjectDetail().get('fake_req', 'fake_pk')

        mock_cs.project_getinfo.assert_called_with('fake_pk')
        mock_re.assert_called_with(
            {'detail': 'fake_exception'}, status=500)

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.ProjectSerializer')
    @mock.patch('cornerstone.rest.v1.views.ProjectUpdateSerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_putProjectDetail(self, mock_re, mock_pus, mock_ps, mock_cs):
        m = mock.Mock(data={'name': 'fake_name',
                            'description': 'fake_desc',
                            'enabled': 'fake_enabled',
                            'owner': 'fake_owner'})
        m.is_valid.return_value = True
        mock_pus.return_value = m
        mock_ps.return_value = mock.Mock(data='ps_fake_data')
        mock_cs.project_update.return_value = 'fake_project'
        views.ProjectDetail().put(mock.Mock(data='fake_data'), 'fake_pk')

        mock_pus.assert_called_with(data='fake_data')
        mock_cs.project_update.assert_called_with(
            project_id='fake_pk',
            name='fake_name',
            description='fake_desc',
            enabled='fake_enabled',
            owner='fake_owner')
        mock_re.assert_called_with('ps_fake_data')

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.ProjectUpdateSerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_putProjectDetail_no_project(
            self, mock_re, mock_pus, mock_cs):
        m = mock.Mock(data={'name': 'fake_name',
                            'description': 'fake_desc',
                            'enabled': 'fake_enabled',
                            'owner': 'fake_owner'})
        m.is_valid.return_value = True
        mock_pus.return_value = m
        mock_cs.project_update.return_value = None
        views.ProjectDetail().put(mock.Mock(data='fake_data'), 'fake_pk')

        mock_pus.assert_called_with(data='fake_data')
        mock_cs.project_update.assert_called_with(
            project_id='fake_pk',
            name='fake_name',
            description='fake_desc',
            enabled='fake_enabled',
            owner='fake_owner')
        mock_re.assert_called_with(
            {'detail': 'Project update failed on "fake_pk"'}, status=412)

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.ProjectUpdateSerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_utProjectDetail_exception(self, mock_re, mock_pus, mock_cs):
        m = mock.MagicMock()
        m.is_valid.return_value = True
        mock_pus.side_effect = m
        mock_cs.project_update.side_effect = Exception('fake_exception')
        views.ProjectDetail().put(mock.Mock(data='fake_data'), 'fake_pk')

        mock_pus.assert_called_with(data='fake_data')
        mock_re.assert_called_with({'detail': 'fake_exception'}, status=500)

    @mock.patch('cornerstone.rest.v1.views.ProjectUpdateSerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_putProjectDetail_no_valid(self, mock_re, mock_pus):
        m = mock.Mock(errors='fake_errors')
        m.is_valid.return_value = False
        mock_pus.return_value = m
        views.ProjectDetail().put(mock.Mock(data='fake_data'), 'fake_pk')

        mock_pus.assert_called_with(data='fake_data')
        mock_re.assert_called_with('fake_errors', status=400)

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_deleteProjectDetail(self, mock_re, mock_cs):
        views.ProjectDetail().delete('fake_req', 'fake_pk')

        mock_cs.project_delete.assert_called_with('fake_pk')
        mock_re.assert_called_with(status=204)

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_deleteProjectDetail_no_output(self, mock_re, mock_cs):
        mock_cs.project_delete.return_value = None
        views.ProjectDetail().delete('fake_req', 'fake_pk')

        mock_cs.project_delete.assert_called_with('fake_pk')
        mock_re.assert_called_with(
            {'detail': 'Project delete failed on "fake_pk"'}, status=412)

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_deleteProjectDetail_exception(self, mock_re, mock_cs):
        mock_cs.project_delete.side_effect = Exception('fake_exception')
        views.ProjectDetail().delete('fake_req', 'fake_pk')

        mock_cs.project_delete.assert_called_with('fake_pk')
        mock_re.assert_called_with(
            {'detail': 'fake_exception'}, status=500)

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.ServiceSerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_getServiceList(self, mock_re, mock_ss, mock_cs):
        mock_cs.service_list.return_value = 'fake_services'
        mock_ss.return_value = mock.Mock(data='fake_data')
        views.ServiceList().get('fake_req')

        mock_cs.service_list.assert_called()
        mock_ss.assert_called_with('fake_services', many=True)
        mock_re.assert_called_with('fake_data')

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.ServiceSerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_getServiceList_exception(self, mock_re, mock_ss, mock_cs):
        mock_cs.service_list.return_value = 'fake_services'
        mock_ss.side_effect = IOError(
            -1, 'f_stderror', 'f_filename')
        views.ServiceList().get('fake_req')

        mock_cs.service_list.assert_called()
        mock_ss.assert_called_with('fake_services', many=True)
        mock_re.assert_called_with(
            {'detail': 'f_stderror: f_filename'}, status=400)

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.ServiceAssignmentSerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_getServiceAssignmentList(self, mock_re, mock_ss, mock_cs):
        mock_cs.project_service_list.return_value = 'fake_services'
        mock_ss.return_value = mock.Mock(data='fake_data')
        views.ServiceAssignmentList().get('fake_req', 'fake_pk')

        mock_cs.project_service_list.assert_called_with('fake_pk')
        mock_ss.assert_called_with('fake_services', many=True)
        mock_re.assert_called_with('fake_data')

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.ServiceAssignmentSerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_postdeleteServiceAssignmentList(
            self, mock_re, mock_ss, mock_cs):
        m = mock.Mock(data={'service_name': 'fake_name'})
        m.is_valid.return_value = True
        mock_ss.return_value = m
        mock_cs.allow_service.return_value = 'fake_ret'
        views.ServiceAssignmentList().postdelete(
            'fake_enabled', 'fake_p_id', None, m)

        mock_ss.assert_called_with(data={'service_name': 'fake_name'})
        mock_cs.allow_service.assert_called_with(
            'fake_name', 'fake_enabled', 'fake_p_id')
        mock_re.assert_called_with({'service_name': 'fake_name'}, status=201)

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.ServiceAssignmentSerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_postdeleteServiceAssignmentList_servicename(
            self, mock_re, mock_ss, mock_cs):
        mock_cs.allow_service.return_value = None

        views.ServiceAssignmentList().postdelete(
            'fake_enabled', 'fake_p_id', 'fake_name', 'fake_req')

        mock_cs.allow_service.assert_called_with(
            'fake_name', 'fake_enabled', 'fake_p_id')
        mock_re.assert_called_with(
            {'detail': "Service allow/disallow failed"}, status=412)

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.ServiceAssignmentSerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_postdeleteServiceAssignmentList_no_enabled(
            self, mock_re, mock_ss, mock_cs):
        m = mock.Mock(data={'service_name': 'fake_name'})
        m.is_valid.return_value = True
        mock_ss.return_value = m
        mock_cs.allow_service.return_value = 'fake_ret'
        views.ServiceAssignmentList().postdelete(
            None, 'fake_p_id', None, m)

        mock_ss.assert_called_with(data={'service_name': 'fake_name'})
        mock_cs.allow_service.assert_called_with(
            'fake_name', None, 'fake_p_id')
        mock_re.assert_called_with({'service_name': 'fake_name'}, status=204)

    @mock.patch('cornerstone.rest.v1.views.ServiceAssignmentSerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_postdeleteServiceAssignmentList_no_valid(self, mock_re,
                                                      mock_ss):
        m = mock.Mock(data={'service_name': 'fake_name'},
                      errors='fake_errors')
        m.is_valid.return_value = False
        mock_ss.return_value = m
        views.ServiceAssignmentList().postdelete(
            None, None, None, m)

        mock_ss.assert_called_with(data={'service_name': 'fake_name'})
        mock_re.assert_called_with('fake_errors', status=400)

    @mock.patch('cornerstone.rest.v1.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v1.views.ServiceAssignmentSerializer')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_postdeleteServiceAssignmentList_exception(
            self, mock_re, mock_ss, mock_cs):
        m = mock.Mock(data={'service_name': 'fake_name'})
        m.is_valid.return_value = True
        mock_ss.return_value = m
        mock_cs.allow_service.side_effect = Exception('fake_exception')
        views.ServiceAssignmentList().postdelete(
            'fake_enabled', 'fake_p_id', None, m)

        mock_ss.assert_called_with(data={'service_name': 'fake_name'})
        mock_cs.allow_service.assert_called_with(
            'fake_name', 'fake_enabled', 'fake_p_id')
        mock_re.assert_called_with({'detail': 'fake_exception'}, status=500)

    @mock.patch('cornerstone.rest.v1.views.ServiceAssignmentList.postdelete')
    def test_deleteServiceAssignmentList(self, mock_pd):
        views.ServiceAssignmentList().delete('fake_req', 'fake_pk')

        mock_pd.assert_called_with(False, 'fake_pk', None, 'fake_req', None)

    @mock.patch('cornerstone.rest.v1.views.ServiceAssignmentList.postdelete')
    def test_postServiceAssignmentList(self, mock_pd):
        views.ServiceAssignmentList().post('fake_req', 'fake_pk')

        mock_pd.assert_called_with(True, 'fake_pk', None, 'fake_req', None)

    @mock.patch('cornerstone.rest.v1.views.apps')
    @mock.patch('cornerstone.rest.v1.views.Response')
    def test_getSchemaDetail(self, mock_re, mock_ap):
        m1 = mock.MagicMock(name='fake_name1', properties='fake_p1')
        m1.__name__ = 'Project'
        m2 = mock.MagicMock(name='fake_name2', properties='fake_p2')
        m2.__name__ = 'fake'

        fs = mock.Mock()
        fs.get_internal_type.return_value = 'AutoField'
        fs2 = mock.Mock(name='fake_name2')
        fs2.get_internal_type.return_value = 'CharField'
        fs3 = mock.Mock(name='fake_name3', primary_key=False)
        fs3.get_internal_type.return_value = 'CharField'
        mf = mock.Mock()
        mf.get_fields.return_value = [fs, fs2, fs3]
        m1._meta = mf
        mock_ap.get_models.return_value = [m1, m2]
        views.SchemaDetail().get('fake_req')

        mock_re.assert_called()
        mock_ap.get_models.assert_called()
