import logging
import unittest

from cornerstone.manager import Manager
from unittest import mock


class TestManager(unittest.TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_dummy(self):
        pass

    @mock.patch('cornerstone.driver.mistral.MistralManager.service_enable')
    @mock.patch('cornerstone.driver.mistral.MistralManager.service_disable')
    def test_allow_service_enabled(self, mock_disable, mock_enable):
        Manager().allow_service('fake_n', 'enabled', 'fake_id')
        mock_enable.assert_called_with('fake_n', 'fake_id')
        mock_disable.assert_not_called()

    @mock.patch('cornerstone.driver.mistral.MistralManager.service_enable')
    @mock.patch('cornerstone.driver.mistral.MistralManager.service_disable')
    def test_allow_service_disabled(self, mock_disable, mock_enable):
        Manager().allow_service('fake_n', '', 'fake_id')
        mock_enable.assert_not_called()
        mock_disable.assert_called_with('fake_n', 'fake_id')

    @mock.patch('cornerstone.driver.mistral.MistralManager.add_grant_project')
    def test_add_grant_project(self, mock_srv):
        Manager().add_grant_project('fake_id', 'fake_r', 'fake_m')
        mock_srv.assert_called_with('fake_id', 'fake_r', 'fake_m')

    @mock.patch('cornerstone.driver.mistral.MistralManager'
                '.delete_grant_project')
    def test_delete_grant_project(self, mock_srv):
        Manager().delete_grant_project('fake_id', 'fake_r', 'fake_m')
        mock_srv.assert_called_with('fake_id', 'fake_r', 'fake_m')

    @mock.patch('cornerstone.driver.mistral.MistralManager.domain_list')
    def test_domain_list(self, mock_srv):
        Manager().domain_list()
        mock_srv.assert_called()

    @mock.patch('cornerstone.driver.mistral.MistralManager'
                '.get_project_members')
    def test_get_project_members(self, mock_srv):
        Manager().get_project_members('fake_id', 'fake_rol')
        mock_srv.assert_called_with('fake_id', 'fake_rol')

    @mock.patch('cornerstone.driver.mistral.MistralManager.is_allowed')
    def test_is_allowed(self, mock_srv):
        Manager().is_allowed('fake_id', 'fake_group', 'fake_rol')
        mock_srv.assert_called_with('fake_id', 'fake_group', 'fake_rol')

    @mock.patch('cornerstone.driver.mistral.MistralManager.project_create')
    def test_project_create(self, mock_srv):
        Manager().project_create(
            project_id='fake_id',
            name='fake_n',
            description='fake_desc',
            enabled=True,
            owner='fake_owner')
        mock_srv.assert_called_with(
            project_id='fake_id',
            name='fake_n',
            description='fake_desc',
            owner='fake_owner',
            administrator='',
            type='official',
            status='active',
            enabled=True)

    @mock.patch('cornerstone.driver.mistral.MistralManager.project_delete')
    def test_project_delete(self, mock_srv):
        Manager().project_delete('fake_id')
        mock_srv.assert_called_with('fake_id')

    @mock.patch('cornerstone.driver.mistral.MistralManager.project_update')
    def test_project_update(self, mock_srv):
        Manager().project_update(
            project_id='fake_id',
            name='fake_n',
            description='fake_desc',
            enabled=True,
            owner='fake_owner')
        mock_srv.assert_called_with(
            project_id='fake_id',
            name='fake_n',
            description='fake_desc',
            owner='fake_owner',
            administrator='',
            type='',
            status='',
            enabled=True)

    @mock.patch('cornerstone.driver.mistral.MistralManager.project_get')
    def test_project_get(self, mock_srv):
        Manager().project_get('fake_id')
        mock_srv.assert_called_with('fake_id')

    @mock.patch('cornerstone.driver.mistral.MistralManager.project_getinfo')
    def test_project_getinfo(self, mock_srv):
        Manager().project_getinfo('fake_id')
        mock_srv.assert_called_with('fake_id')

    @mock.patch('cornerstone.driver.mistral.MistralManager.project_list')
    def test_project_list(self, mock_srv):
        Manager().project_list(False)
        mock_srv.assert_called_with(False)

    @mock.patch('cornerstone.driver.mistral.MistralManager'
                '.project_list_groups_grants')
    def test_project_list_groups_grants(self, mock_srv):
        Manager().project_list_groups_grants('fake_id')
        mock_srv.assert_called_with('fake_id')

    @mock.patch('cornerstone.driver.mistral.MistralManager'
                '.project_add_property')
    def test_project_add_property(self, mock_srv):
        Manager().project_add_property('fake_id', 'fake_p_name', 'fake_p_val')
        mock_srv.assert_called_with('fake_id', 'fake_p_name', 'fake_p_val')

    @mock.patch('cornerstone.driver.mistral.MistralManager'
                '.project_delete_property')
    def test_project_delete_property(self, mock_srv):
        Manager().project_delete_property('fake_id', 'fake_p_name')
        mock_srv.assert_called_with('fake_id', 'fake_p_name')

    @mock.patch('cornerstone.driver.mistral.MistralManager'
                '.project_list_property')
    def test_project_list_property(self, mock_srv):
        Manager().project_list_property('fake_id')
        mock_srv.assert_called_with('fake_id')

    @mock.patch('cornerstone.driver.mistral.MistralManager'
                '.project_update_property')
    def test_project_update_property(self, mock_srv):
        Manager().project_update_property('fake_id',
                                          'fake_p_name',
                                          'fake_p_value')
        mock_srv.assert_called_with('fake_id',
                                    'fake_p_name',
                                    'fake_p_value')

    @mock.patch('cornerstone.driver.mistral.MistralManager.service_list')
    def test_service_list(self, mock_srv):
        Manager().service_list()
        mock_srv.assert_called()

    @mock.patch('cornerstone.driver.mistral.MistralManager'
                '.project_service_list')
    def test_project_service_list(self, mock_srv):
        Manager().project_service_list('fake_id')
        mock_srv.assert_called_with('fake_id')

    @mock.patch('cornerstone.driver.mistral.MistralManager'
                '.grant_group_project')
    @mock.patch('cornerstone.driver.mistral.MistralManager'
                '.revoke_group_project')
    def test_set_allow_group_grant(self, mock_revoke, mock_grant):
        Manager().set_allow_group('fake_id', True,
                                  'fake_g_name', 'fake_r_name')
        mock_grant.assert_called_with('fake_id',
                                      'fake_g_name', 'fake_r_name')
        mock_revoke.assert_not_called()

    @mock.patch('cornerstone.driver.mistral.MistralManager'
                '.grant_group_project')
    @mock.patch('cornerstone.driver.mistral.MistralManager'
                '.revoke_group_project')
    def test_set_allow_group_revoke(self, mock_revoke, mock_grant):
        Manager().set_allow_group('fake_id', False,
                                  'fake_g_name', 'fake_r_name')
        mock_revoke.assert_called_with('fake_id',
                                       'fake_g_name', 'fake_r_name')
        mock_grant.assert_not_called()

    @mock.patch('cornerstone.driver.mistral.MistralManager'
                '.set_project_members')
    def test_set_project_members(self, mock_srv):
        Manager().set_project_members('fake_id', 'fake_r_name', 'fake_member')
        mock_srv.assert_called_with('fake_id', 'fake_r_name', 'fake_member')
