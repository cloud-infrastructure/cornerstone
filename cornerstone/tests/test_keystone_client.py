import logging
import unittest

from cornerstone.clients.keystone_client import KeystoneClient
from unittest import mock


@mock.patch('cornerstone.clients.keystone_client.keystone_session')
@mock.patch('cornerstone.clients.keystone_client.keystone_client_v3')
class TestKeystoneClient(unittest.TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_get_role_fail(self, mock_kc, mock_ks):

        with self.assertRaises(Exception):
            KeystoneClient().get_role_by_name('fake_rol')

    def test_get_role(self, mock_kc, mock_ks):
        mock_client = mock_kc.Client.return_value
        mock_client.roles.list.return_value = []
        m = mock.MagicMock()
        m.name = 'fake_rol'
        mock_kc.Client.return_value.roles.list.return_value = [m]

        KeystoneClient().get_role_by_name('fake_rol')

        mock_kc.Client.assert_called()

        mock_client.roles.list.assert_called()

    def test_list_project(self, mock_kc, mock_ks):
        mock_client = mock_kc.Client.return_value
        mock_client.projects.list.return_value = []

        KeystoneClient().list_project()

        mock_kc.Client.assert_called()

        mock_client.projects.list.assert_called()

    def test_list_role_assignments(self, mock_kc, mock_ks):
        mock_client = mock_kc.Client.return_value
        mock_client.role_assignments.list.return_value = []

        KeystoneClient().list_role_assignments()

        mock_kc.Client.assert_called()

        mock_client.role_assignments.list.assert_called()
