import logging
import unittest

from cornerstone.soap import main
from unittest import mock


@mock.patch('cornerstone.soap.main.CMDSERVER')
class TestSoapMain(unittest.TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_add_project_members(self, mock_mn):
        ret = main.add_project_members('fake_id', 'fake_r', 'fake_m')

        mock_mn.add_grant_project.assert_called_with('fake_id',
                                                     'fake_r',
                                                     'fake_m')
        if ret != {'returnCode': 0, 'msg': None}:
            assert False

    def test_add_project_members_fail(self, mock_mn):
        mock_mn.add_grant_project.return_value = None
        ret = main.add_project_members('fake_id', 'fake_r', 'fake_m')

        mock_mn.add_grant_project.assert_called_with('fake_id',
                                                     'fake_r',
                                                     'fake_m')
        if ret != {'returnCode': 1, 'msg': None}:
            assert False

    def test_add_project_members_exception(self, mock_mn):
        mock_mn.add_grant_project.side_effect = Exception

        self.assertRaises(Exception,
                          main.add_project_members(
                              'fake_id', 'fake_r', 'fake_m'))

    def test_allow_service(self, mock_mn):
        ret = main.allow_service('fake_n', 'f_enable', 'fake_id')

        mock_mn.allow_service.assert_called_with('fake_n',
                                                 'f_enable',
                                                 'fake_id')
        if ret != {'returnCode': 0, 'msg': None}:
            assert False

    def test_allow_service_fail(self, mock_mn):
        mock_mn.allow_service.return_value = None
        ret = main.allow_service('fake_n', 'f_enable', 'fake_id')

        mock_mn.allow_service.assert_called_with('fake_n',
                                                 'f_enable',
                                                 'fake_id')
        if ret != {'returnCode': 1, 'msg': None}:
            assert False

    def test_allow_service_exception(self, mock_mn):
        mock_mn.allow_service.side_effect = Exception('fake')

        self.assertRaises(Exception,
                          main.allow_service(
                              'fake_n', 'f_enable', 'fake_id'))

    def test_del_project_members(self, mock_mn):
        ret = main.del_project_members('fake_id', 'f_rol', 'fake_m')

        mock_mn.delete_grant_project.assert_called_with('fake_id',
                                                        'f_rol',
                                                        'fake_m')
        if ret != {'returnCode': 0, 'msg': None}:
            assert False

    def test_del_project_members_fail(self, mock_mn):
        mock_mn.delete_grant_project.return_value = None
        ret = main.del_project_members('fake_id', 'f_rol', 'fake_m')

        mock_mn.delete_grant_project.assert_called_with('fake_id',
                                                        'f_rol',
                                                        'fake_m')
        if ret != {'returnCode': 1, 'msg': None}:
            assert False

    def test_del_project_members_exception(self, mock_mn):
        mock_mn.delete_grant_project.side_effect = Exception('fake')

        self.assertRaises(Exception,
                          main.del_project_members(
                              'fake_id', 'f_rol', 'fake_m'))

    def test_domain_list(self, mock_mn):
        mock_mn.domain_list.return_value = None
        ret = main.domain_list()

        mock_mn.domain_list.assert_called()
        if ret != {'returnCode': 0, 'domainList': None}:
            assert False

    def test_domain_list_exception(self, mock_mn):
        mock_mn.domain_list.side_effect = IOError(-1, 'f_stderror',
                                                  'f_filename')
        self.assertRaises(Exception,
                          main.domain_list())

    def test_get_project_members(self, mock_mn):
        mock_mn.get_project_members.return_value = 'f_member'
        ret = main.get_project_members('fake_id', 'f_rol')

        mock_mn.get_project_members.assert_called_with('fake_id', 'f_rol')
        if ret != {'returnCode': 0, 'memberList': 'f_member'}:
            assert False

    def test_get_project_members_fail(self, mock_mn):
        mock_mn.get_project_members.return_value = None
        ret = main.get_project_members('fake_id', 'f_rol')

        mock_mn.get_project_members.assert_called_with('fake_id', 'f_rol')
        if ret != {'returnCode': 0, 'memberList': [{'name': 'empty'}]}:
            assert False

    def test_get_project_members_exception(self, mock_mn):
        mock_mn.get_project_members.side_effect = Exception

        self.assertRaises(Exception,
                          main.get_project_members(
                              'fake_id', 'f_rol'))

    def test_get_project_info(self, mock_mn):
        mock_mn.project_getinfo.return_value = {
            'services': [{'service_name': 'fake1'}]
        }
        ret = main.get_project_info('fake_id')

        mock_mn.project_getinfo.assert_called_with('fake_id')
        if ret != {'returnCode': 0, 'projectInfo': {'services': ['fake1']}}:
            assert False

    def test_get_project_info_fail(self, mock_mn):
        mock_mn.project_getinfo.return_value = None
        ret = main.get_project_info('fake_id')

        mock_mn.project_getinfo.assert_called_with('fake_id')
        if ret != {'returnCode': 1, 'project': None}:
            assert False

    def test_get_project_info_exception(self, mock_mn):
        mock_mn.project_getinfo.side_effect = Exception

        self.assertRaises(Exception,
                          main.get_project_info(
                              'fake_id'))

    def test_project_create(self, mock_mn):
        mock_mn.project_create.return_value = 'fake1'
        ret = main.project_create('fake_id',
                                  'fake_n',
                                  'fake_d',
                                  'fake_e',
                                  'fake_o')

        mock_mn.project_create.assert_called_with(
            project_id='fake_id',
            name='fake_n',
            description='fake_d',
            enabled='fake_e',
            owner='fake_o')
        if ret != {'returnCode': 0, 'project': 'fake1'}:
            assert False

    def test_project_create_fail(self, mock_mn):
        mock_mn.project_create.return_value = None
        ret = main.project_create('fake_id',
                                  'fake_n',
                                  'fake_d',
                                  'fake_e',
                                  'fake_o')

        mock_mn.project_create.assert_called_with(
            project_id='fake_id',
            name='fake_n',
            description='fake_d',
            enabled='fake_e',
            owner='fake_o')
        if ret != {'returnCode': -1, 'project': None}:
            assert False

    def test_project_create_fail2(self, mock_mn):
        mock_mn.project_create.return_value = None
        ret = main.project_create('fake_id',
                                  '',
                                  'fake_d',
                                  'fake_e',
                                  'fake_o')

        mock_mn.project_create.assert_not_called()
        if ret != {'returnCode': 1, 'project': None}:
            assert False

    def test_project_create_exception(self, mock_mn):
        mock_mn.project_create.side_effect = Exception

        self.assertRaises(Exception,
                          main.project_create(
                              'fake_id', 'fake_n', 'fake_d',
                              'fake_e', 'fake_o'))

    def test_project_delete(self, mock_mn):
        mock_mn.project_delete.return_value = 'fake_1'
        ret = main.project_delete('fake_id')

        mock_mn.project_delete.assert_called_with('fake_id')
        if ret != {'returnCode': 0, 'project': None}:
            assert False

    def test_project_delete_fail(self, mock_mn):
        mock_mn.project_delete.return_value = None
        ret = main.project_delete('fake_id')

        mock_mn.project_delete.assert_called_with('fake_id')
        if ret != {'returnCode': -1, 'project': None}:
            assert False

    def test_project_delete_fail2(self, mock_mn):
        mock_mn.project_delete.return_value = None
        ret = main.project_delete('')

        mock_mn.project_delete.assert_not_called()
        if ret != {'returnCode': 1, 'project': None}:
            assert False

    def test_project_delete_exception(self, mock_mn):
        mock_mn.project_delete.side_effect = Exception

        self.assertRaises(Exception,
                          main.project_delete(
                              'fake_id'))

    def test_project_get(self, mock_mn):
        mock_mn.project_get.return_value = {
            'allowOperators': [{'fake': 'fake1'}],
            'allowSupporters': [{'fake': 'fake1'}],
            'services': [{'fake': 'fake1'}]
        }
        ret = main.project_get('fake_id')

        mock_mn.project_get.assert_called_with('fake_id')
        if ret != {'returnCode': 0, 'projectInfo': {}}:
            assert False

    def test_project_get_fail(self, mock_mn):
        mock_mn.project_get.return_value = None
        ret = main.project_get('fake_id')

        mock_mn.project_get.assert_called_with('fake_id')
        if ret != {'returnCode': 1, 'project': None}:
            assert False

    def test_project_get_fail2(self, mock_mn):
        mock_mn.project_get.return_value = None
        ret = main.project_get('')

        mock_mn.project_get.assert_not_called()
        if ret != {'returnCode': 1, 'project': None}:
            assert False

    def test_project_get_exception(self, mock_mn):
        mock_mn.project_get.side_effect = Exception

        self.assertRaises(Exception,
                          main.project_get(
                              'fake_id'))

    def test_project_list(self, mock_mn):
        mock_mn.project_list.return_value = ['fake1', 'fake2']
        ret = main.project_list()

        mock_mn.project_list.assert_called()
        if ret != {'returnCode': 0, 'projectList': ['fake1', 'fake2']}:
            assert False

    def test_project_list_fail(self, mock_mn):
        mock_mn.project_list.return_value = None
        ret = main.project_list()

        mock_mn.project_list.assert_called()
        if ret != {'returnCode': -1, 'projectList': []}:
            assert False

    def test_project_list_exception(self, mock_mn):
        mock_mn.project_list.side_effect = Exception

        self.assertRaises(Exception,
                          main.project_list())

    def test_project_update(self, mock_mn):
        mock_mn.project_update.return_value = 'fake_ok'
        ret = main.project_update('fake_id',
                                  'fake_n',
                                  'fake_d',
                                  'fake_e',
                                  'fake_o')

        mock_mn.project_update.assert_called_with(
            project_id='fake_id',
            name='fake_n',
            description='fake_d',
            enabled='fake_e',
            owner='fake_o')
        if ret != {'returnCode': 0, 'project': 'fake_ok'}:
            assert False

    def test_project_update_fail(self, mock_mn):
        mock_mn.project_update.return_value = None
        ret = main.project_update('fake_id',
                                  'fake_n',
                                  'fake_d',
                                  'fake_e',
                                  'fake_o')

        mock_mn.project_update.assert_called_with(
            project_id='fake_id',
            name='fake_n',
            description='fake_d',
            enabled='fake_e',
            owner='fake_o')
        if ret != {'returnCode': -1, 'project': None}:
            assert False

    def test_project_update_fail2(self, mock_mn):
        mock_mn.project_update.return_value = None
        ret = main.project_update('',
                                  'fake_n',
                                  'fake_d',
                                  'fake_e',
                                  'fake_o')

        mock_mn.project_update.assert_not_called()
        if ret != {'returnCode': 1, 'project': None}:
            assert False

    def test_project_update_exception(self, mock_mn):
        mock_mn.project_update.side_effect = Exception

        self.assertRaises(Exception,
                          main.project_update(
                              'fake_id', 'fake_n', 'fake_d',
                              'fake_e', 'fake_o'))

    def test_set_allow_operators(self, mock_mn):
        mock_mn.is_allowed.return_value = 'fake_allow'
        mock_mn.set_allow_group.return_value = 'fake_allow_g'
        ret = main.set_allow_operators('fake_id', 'fake_v')

        mock_mn.is_allowed.assert_called_with(group_name='operator',
                                              project_id='fake_id',
                                              role_name='operator')
        mock_mn.set_allow_group.assert_not_called()
        if ret != {'returnCode': 0, 'msg': None}:
            assert False

    def test_set_allow_operators_fail(self, mock_mn):
        mock_mn.is_allowed.return_value = None
        ret = main.set_allow_operators('fake_id', 'fake_v')

        mock_mn.is_allowed.assert_called_with(group_name='operator',
                                              project_id='fake_id',
                                              role_name='operator')
        mock_mn.set_allow_group.assert_called_with(group_name='operator',
                                                   project_id='fake_id',
                                                   role_name='operator',
                                                   value='fake_v')
        if ret != {'returnCode': 0, 'msg': None}:
            assert False

    def test_set_allow_operators_fail2(self, mock_mn):
        mock_mn.is_allowed.return_value = None
        mock_mn.set_allow_group.return_value = None
        ret = main.set_allow_operators('fake_id', 'fake_v')

        mock_mn.is_allowed.assert_called_with(group_name='operator',
                                              project_id='fake_id',
                                              role_name='operator')
        mock_mn.set_allow_group.assert_called_with(group_name='operator',
                                                   project_id='fake_id',
                                                   role_name='operator',
                                                   value='fake_v')
        if ret != {'returnCode': 1, 'msg': None}:
            assert False

    def test_set_allow_operators_exception(self, mock_mn):
        mock_mn.set_allow_group.side_effect = Exception('fake')

        self.assertRaises(Exception,
                          main.set_allow_operators('', ''))

    def test_set_project_members(self, mock_mn):
        mock_mn.set_project_members.return_value = 'fake_set'
        ret = main.set_project_members('fake_id', 'fake_r', 'fake_m')

        mock_mn.set_project_members.assert_called_with('fake_id',
                                                       'fake_r',
                                                       'fake_m')
        if ret != {'returnCode': 0, 'msg': None}:
            assert False

    def test_set_project_members_fail(self, mock_mn):
        mock_mn.set_project_members.return_value = None
        ret = main.set_project_members('fake_id', 'fake_r', 'fake_m')

        mock_mn.set_project_members.assert_called_with('fake_id',
                                                       'fake_r',
                                                       'fake_m')
        if ret != {'returnCode': 1, 'msg': None}:
            assert False

    def test_set_project_members_exception(self, mock_mn):
        mock_mn.set_project_members.side_effect = Exception('fake')

        self.assertRaises(Exception,
                          main.set_project_members(
                              'fake_id', 'fake_r', 'fake_m'))
