import logging
import unittest

from cornerstone.clients.mistral_client import MistralClient
from unittest import mock


@mock.patch('cornerstone.clients.mistral_client.mistral_client')
class TestMistralClient(unittest.TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_workflows_list(self, mock_mc):
        mock_client = mock_mc.client.return_value
        mock_session = mock.Mock()

        MistralClient(
            session=mock_session,
            mistral_url='fake_url').workflow_list()

        mock_mc.client.assert_called_with(
            session=mock_session,
            mistral_url='fake_url')

        mock_client.workflows.list.assert_called()

    def test_workflow_get(self, mock_mc):
        mock_client = mock_mc.client.return_value
        mock_session = mock.Mock()

        MistralClient(
            session=mock_session,
            mistral_url='fake_url').workflow_get('fake_id')

        mock_mc.client.assert_called_with(
            session=mock_session,
            mistral_url='fake_url')

        mock_client.workflows.get.assert_called()

    def test_execution_list(self, mock_mc):
        mock_client = mock_mc.client.return_value
        mock_session = mock.Mock()

        MistralClient(
            session=mock_session,
            mistral_url='fake_url').execution_list()

        mock_mc.client.assert_called_with(
            session=mock_session,
            mistral_url='fake_url')

        mock_client.executions.list.assert_called_with()

    def test_execution_create(self, mock_mc):
        mock_client = mock_mc.client.return_value
        mock_session = mock.Mock()

        MistralClient(
            session=mock_session,
            mistral_url='fake_url').execution_create(
                'fake_id', 'fake_input', 'fake_desc')

        mock_mc.client.assert_called_with(
            session=mock_session,
            mistral_url='fake_url')

        mock_client.executions.create.assert_called_with(
            description='fake_desc',
            wf_identifier='fake_id',
            workflow_input='fake_input')

    def test_execution_get(self, mock_mc):
        mock_client = mock_mc.client.return_value
        mock_session = mock.Mock()

        MistralClient(
            session=mock_session,
            mistral_url='fake_url').execution_get('fake_id')

        mock_mc.client.assert_called_with(
            session=mock_session,
            mistral_url='fake_url')

        mock_client.executions.get.assert_called_with('fake_id')

    @mock.patch('cornerstone.clients.mistral_client.'
                'MistralClient.execution_create')
    def test_execution_create_async(self, mock_ec, mock_mc):
        mock_session = mock.Mock()

        MistralClient(
            session=mock_session,
            mistral_url='fake_url').execution_create_async(
                'fake_id', 'fake_input', 'fake_desc')

        mock_mc.client.assert_called_with(
            session=mock_session,
            mistral_url='fake_url')

        mock_ec.assert_called_with(description='fake_desc',
                                   workflow_identifier='fake_id',
                                   workflow_input='fake_input')

    @mock.patch('cornerstone.clients.mistral_client'
                '.MistralClient.execution_create')
    def test_execution_create_async_exception(self, mock_ec, mock_mc):
        from cornerstone.clients.mistral_client import APIException
        mock_session = mock.Mock()

        mock_ec.side_effect = APIException

        self.assertRaises(Exception,
                          MistralClient(
                              session=mock_session,
                              mistral_url='fake_url').execution_create_async(
                                  'fake_id', 'fake_input', 'fake_desc'))

        mock_mc.client.assert_called_with(
            session=mock_session,
            mistral_url='fake_url')

    @mock.patch('cornerstone.clients.mistral_client'
                '.MistralClient.execution_get')
    @mock.patch('cornerstone.clients.mistral_client.json')
    def test_execution_create_sync(self, mock_js, mock_mm, mock_mc):
        mock_session = mock.Mock()
        m = mock.Mock()
        m.state = u'SUCCESS'
        mock_mm.return_value = m
        mock_js.loads.side_effect = ValueError

        MistralClient(
            session=mock_session,
            mistral_url='fake_url').execution_create_sync(
                'fake_w_ou',
                'fake_w_in',
                'fake_des', 0, 1)

        mock_mc.client.assert_called_with(
            session=mock_session,
            mistral_url='fake_url')

    @mock.patch('cornerstone.clients.mistral_client'
                '.MistralClient.execution_get')
    def test_execution_create_sync_fail_exception(self, mock_mm, mock_mc):
        from cornerstone.clients.mistral_client import APIException
        mock_session = mock.Mock()

        mock_mm.side_effect = APIException

        self.assertRaises(Exception,
                          MistralClient(
                              session=mock_session,
                              mistral_url='fake_url').execution_create_sync(
                                  'fake_w_ou',
                                  'fake_w_in',
                                  'fake_des', 0, 1))

        mock_mc.client.assert_called_with(
            session=mock_session,
            mistral_url='fake_url')

    @mock.patch('cornerstone.clients.mistral_client'
                '.MistralClient.execution_get')
    def test_execution_create_sync_fail(self, mock_mm, mock_mc):
        mock_session = mock.Mock()
        m = mock.Mock()
        m.state = u'RUNNING'
        mock_mm.return_value = m

        MistralClient(
            session=mock_session,
            mistral_url='fake_url').execution_create_sync(
                'fake_w_ou',
                'fake_w_in',
                'fake_des', 0, 1)

        mock_mc.client.assert_called_with(
            session=mock_session,
            mistral_url='fake_url')

    @mock.patch('cornerstone.clients.mistral_client'
                '.MistralClient.execution_get')
    def test_execution_create_sync_fail_json(self, mock_mm, mock_mc):
        mock_session = mock.Mock()
        m = mock.Mock()
        m.state = u'RUNNING'
        mock_mm.return_value = m

        MistralClient(
            session=mock_session,
            mistral_url='fake_url').execution_create_sync(
                'fake_w_ou',
                'fake_w_in',
                'fake_des', 0, 1)

        mock_mc.client.assert_called_with(
            session=mock_session,
            mistral_url='fake_url')
