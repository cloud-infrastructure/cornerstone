import logging
import unittest

from cornerstone import hooks
from unittest import mock


@mock.patch('cornerstone.hooks.install')
class TestHooks(unittest.TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_setup_hook(self, mock_in):
        m = mock.MagicMock()
        mock_in.INSTALL_SCHEMES.values.return_value = [{'data': 'fake_data',
                                                        'purelib': 'fake_lib'}]
        hooks.setup_hook(m)

        mock_in.INSTALL_SCHEMES.values.assert_called()
