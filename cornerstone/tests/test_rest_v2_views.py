import logging
import unittest

from cornerstone.rest.v2 import views
from unittest import mock


class TestRestV2Views(unittest.TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    @mock.patch('cornerstone.rest.v2.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v2.views.ProjectSerializer')
    @mock.patch('cornerstone.rest.v2.views.Response')
    def test_getProjectList(self, mock_re, mock_ps, mock_cs):
        mock_cs.project_list.return_value = 'fake_projects'
        mock_ps.return_value = mock.Mock(data='fake_data')
        views.ProjectList().get('fake_req')

        mock_cs.project_list.assert_called()
        mock_ps.assert_called_with('fake_projects', many=True)
        mock_re.assert_called_with(data={
            'pagination': {},
            'data': 'fake_data'
        })

    @mock.patch('cornerstone.rest.v2.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v2.views.ProjectSerializer')
    @mock.patch('cornerstone.rest.v2.views.Response')
    def test_postProjectList(self, mock_re, mock_ps, mock_cs):
        m = mock.Mock(data={
            'id': 'fake_id',
            'name': 'fake_name',
            'description': 'fake_desc',
            'type': 'fake_type',
            'status': 'fake_status',
            'administrator': 'fake_administrator',
            'owner': 'fake_owner'})
        m.is_valid.return_value = True
        mock_ps.return_value = m
        views.ProjectList().post(mock.Mock(data='fake_req'))

        mock_ps.assert_called_with(data='fake_req')
        mock_cs.project_create.assert_called_with(
            project_id='fake_id',
            name='fake_name',
            description='fake_desc',
            type='fake_type',
            status='fake_status',
            administrator='fake_administrator',
            owner='fake_owner')
        mock_re.assert_called_with(data={
            'data': {
                'id': 'fake_id',
                'name': 'fake_name',
                'description': 'fake_desc',
                'type': 'fake_type',
                'status': 'fake_status',
                'administrator': 'fake_administrator',
                'owner': 'fake_owner'
            }}, status=201)

    @mock.patch('cornerstone.rest.v2.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v2.views.ProjectSerializer')
    @mock.patch('cornerstone.rest.v2.views.Response')
    def test_postProjectList_no_project(self, mock_re, mock_ps, mock_cs):
        m = mock.Mock(data={
            'id': 'fake_id',
            'name': 'fake_name',
            'description': 'fake_desc',
            'type': 'fake_type',
            'status': 'fake_status',
            'administrator': 'fake_administrator',
            'owner': 'fake_owner'})
        m.is_valid.return_value = True
        mock_ps.return_value = m
        mock_cs.project_create.return_value = None
        views.ProjectList().post(mock.Mock(data='fake_req'))

        mock_ps.assert_called_with(data='fake_req')
        mock_re.assert_called_with(
            {'detail': 'Project create failed on "fake_id"'}, status=412)

    @mock.patch('cornerstone.rest.v2.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v2.views.ProjectSerializer')
    @mock.patch('cornerstone.rest.v2.views.Response')
    def test_postProjectList_except1(self, mock_re, mock_ps, mock_cs):
        m = mock.MagicMock()
        m.is_valid.return_value = True
        mock_ps.return_value = m
        mock_cs.project_create.side_effect = Exception('fail_project')
        views.ProjectList().post(mock.Mock(data='fake_req'))

        mock_ps.assert_called_with(data='fake_req')
        mock_re.assert_called_with(
            {'detail': 'fail_project'}, status=500)

    @mock.patch('cornerstone.rest.v2.views.ProjectSerializer')
    @mock.patch('cornerstone.rest.v2.views.Response')
    def test_postProjectList_except2(self, mock_re, mock_ps):
        m = mock.Mock(errors='fake_errors')
        m.is_valid.return_value = False
        mock_ps.return_value = m
        views.ProjectList().post(mock.Mock(data='fake_req'))

        mock_ps.assert_called_with(data='fake_req')
        mock_re.assert_called_with('fake_errors', status=400)

    @mock.patch('cornerstone.rest.v2.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v2.views.ProjectSerializer')
    @mock.patch('cornerstone.rest.v2.views.Response')
    def test_getProjectDetail(self, mock_re, mock_ps, mock_cs):
        mock_cs.project_get.return_value = 'fake_project'
        mock_ps.return_value = mock.Mock(data='fake_data')
        views.ProjectDetail().get('fake_req', 'fake_pk')

        mock_cs.project_get.assert_called_with('fake_pk')
        mock_ps.assert_called_with('fake_project')
        mock_re.assert_called_with(data={'data': 'fake_data'})

    @mock.patch('cornerstone.rest.v2.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v2.views.Response')
    def test_getProjectDetail_no_project(self, mock_re, mock_cs):
        mock_cs.project_get.return_value = None
        views.ProjectDetail().get('fake_req', 'fake_pk')

        mock_cs.project_get.assert_called_with('fake_pk')
        mock_re.assert_called_with(
            {'detail': 'Project get failed on "fake_pk"'}, status=412)

    @mock.patch('cornerstone.rest.v2.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v2.views.Response')
    def test_getProjectDetail_exception(self, mock_re, mock_cs):
        mock_cs.project_get.side_effect = Exception('fake_exception')
        views.ProjectDetail().get('fake_req', 'fake_pk')

        mock_cs.project_get.assert_called_with('fake_pk')
        mock_re.assert_called_with(
            {'detail': 'fake_exception'}, status=500)

    @mock.patch('cornerstone.rest.v2.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v2.views.ProjectSerializer')
    @mock.patch('cornerstone.rest.v2.views.ProjectUpdateSerializer')
    @mock.patch('cornerstone.rest.v2.views.Response')
    def test_putProjectDetail(self, mock_re, mock_pus, mock_ps, mock_cs):
        m = mock.Mock(data={
            'id': 'fake_pk',
            'name': 'fake_name',
            'description': 'fake_desc',
            'type': 'fake_type',
            'status': 'fake_status',
            'owner': 'fake_owner'})
        m.is_valid.return_value = True
        mock_pus.return_value = m
        mock_ps.return_value = m
        mock_cs.project_update.return_value = 'fake_project'
        views.ProjectDetail().put(mock.Mock(data='fake_data'), 'fake_pk')

        mock_cs.project_update.assert_called_with(
            project_id='fake_pk',
            name='fake_name',
            description='fake_desc',
            type='fake_type',
            status='fake_status',
            administrator=None,
            owner='fake_owner')
        mock_re.assert_called_with(data={'data': m.data})

    @mock.patch('cornerstone.rest.v2.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v2.views.ProjectUpdateSerializer')
    @mock.patch('cornerstone.rest.v2.views.Response')
    def test_putProjectDetail_no_project(
            self, mock_re, mock_pus, mock_cs):
        m = mock.Mock(data={
            'id': 'fake_pk',
            'name': 'fake_name',
            'description': 'fake_desc',
            'type': 'fake_type',
            'status': 'fake_status',
            'owner': 'fake_owner'})
        m.is_valid.return_value = True
        mock_pus.return_value = m
        mock_cs.project_update.return_value = None
        views.ProjectDetail().put(mock.Mock(data='fake_data'), 'fake_pk')

        mock_pus.assert_called_with(data='fake_data')
        mock_cs.project_update.assert_called_with(
            project_id='fake_pk',
            name='fake_name',
            description='fake_desc',
            type='fake_type',
            status='fake_status',
            administrator=None,
            owner='fake_owner')
        mock_re.assert_called_with(
            {'detail': 'Project update failed on "fake_pk"'}, status=412)

    @mock.patch('cornerstone.rest.v2.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v2.views.ProjectUpdateSerializer')
    @mock.patch('cornerstone.rest.v2.views.Response')
    def test_putProjectDetail_exception(self, mock_re, mock_pus, mock_cs):
        m = mock.MagicMock()
        m.is_valid.return_value = True
        mock_pus.side_effect = m
        mock_cs.project_update.side_effect = Exception('fake_exception')
        views.ProjectDetail().put(mock.Mock(data='fake_data'), 'fake_pk')

        mock_pus.assert_called_with(data='fake_data')
        mock_re.assert_called_with({'detail': 'fake_exception'}, status=500)

    @mock.patch('cornerstone.rest.v2.views.ProjectUpdateSerializer')
    @mock.patch('cornerstone.rest.v2.views.Response')
    def test_putProjectDetail_no_valid(self, mock_re, mock_pus):
        m = mock.Mock(errors='fake_errors')
        m.is_valid.return_value = False
        mock_pus.return_value = m
        views.ProjectDetail().put(mock.Mock(data='fake_data'), 'fake_pk')

        mock_pus.assert_called_with(data='fake_data')
        mock_re.assert_called_with('fake_errors', status=400)

    @mock.patch('cornerstone.rest.v2.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v2.views.Response')
    def test_deleteProjectDetail(self, mock_re, mock_cs):
        views.ProjectDetail().delete('fake_req', 'fake_pk')

        mock_cs.project_delete.assert_called_with('fake_pk')
        mock_re.assert_called_with(status=204)

    @mock.patch('cornerstone.rest.v2.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v2.views.Response')
    def test_deleteProjectDetail_no_output(self, mock_re, mock_cs):
        mock_cs.project_delete.return_value = None
        views.ProjectDetail().delete('fake_req', 'fake_pk')

        mock_cs.project_delete.assert_called_with('fake_pk')
        mock_re.assert_called_with(
            {'detail': 'Project delete failed on "fake_pk"'}, status=412)

    @mock.patch('cornerstone.rest.v2.views.APIViewCommandServer.cmdServer')
    @mock.patch('cornerstone.rest.v2.views.Response')
    def test_deleteProjectDetail_exception(self, mock_re, mock_cs):
        mock_cs.project_delete.side_effect = Exception('fake_exception')
        views.ProjectDetail().delete('fake_req', 'fake_pk')

        mock_cs.project_delete.assert_called_with('fake_pk')
        mock_re.assert_called_with(
            {'detail': 'fake_exception'}, status=500)

    @mock.patch('cornerstone.rest.v2.views.apps')
    @mock.patch('cornerstone.rest.v2.views.Response')
    def test_getSchemaDetail(self, mock_re, mock_ap):
        m1 = mock.MagicMock(name='fake_name1', properties='fake_p1')
        m1.__name__ = 'project'
        m2 = mock.MagicMock(name='fake_name2', properties='fake_p2')
        m2.__name__ = 'fake'

        fs = mock.Mock()
        fs.get_internal_type.return_value = 'AutoField'
        fs2 = mock.Mock(name='fake_name2')
        fs2.get_internal_type.return_value = 'CharField'
        fs3 = mock.Mock(name='fake_name3', primary_key=False)
        fs3.get_internal_type.return_value = 'CharField'
        mf = mock.Mock()
        mf.get_fields.return_value = [fs, fs2, fs3]
        m1._meta = mf
        mock_ap.get_models.return_value = [m1, m2]
        views.SchemaDetail().get('fake_req')

        mock_re.assert_called()
        mock_ap.get_models.assert_called()
