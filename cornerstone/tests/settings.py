import django

# Django settings for cornerstone project.
DEBUG = False
TEMPLATE_DEBUG = DEBUG

TIME_ZONE = 'Europe/Zurich'
LANGUAGE_CODE = 'en-us'

SITE_ID = 1
USE_I18N = True
USE_L10N = True
USE_TZ = True
MEDIA_ROOT = ''
MEDIA_URL = ''
STATIC_ROOT = ''
STATIC_URL = '/static/'
STATICFILES_DIRS = ()

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

SECRET_KEY = 'SECRETKEY'  # nosec

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

ROOT_URLCONF = 'cornerstone.urls'

WSGI_APPLICATION = 'cornerstone.wsgi.application'

TEMPLATE_DIRS = ()

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.AllowAny',
    ),
    'DEFAULT_PAGINATION_CLASS': ('rest_framework.pagination.'
                                 'PageNumberPagination'),
    'PAGE_SIZE': 100
}

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'cornerstone.rest',
    'cornerstone.soap',
)

# ALLOWED_HOSTS needed for DEBUG=False in py1.7
# See https://docs.djangoproject.com/en/1.7/ref/settings/#debug
ALLOWED_HOSTS = ['*']

DATABASES = {'default': {'ENGINE': 'django.db.backends.sqlite3'}}

ADMIN_NAME = 'admin_name'
ADMIN_PASS = 'admin_password'  # nosec
ADMIN_PROJECT = 'admin_project'
AUTH_URL = 'http://localhost:5000/v3/'
SERVICE_NAME = 'Cornerstone'
SERVICE_LOCATION = 'http://localhost/api/'
OWNER_ROLE_NAME = 'owner'
KEYSTONE_RETRIES = 100
KEYSTONE_TIMEOUT = 2
GROUP_OPERATORS = 'operator'
ROLE_OPERATORS = 'operator'
GROUP_SUPPORTERS = 'support'
ROLE_SUPPORTERS = 'support'
DOMAINS = [
    'Default'
]
MISTRAL_RETRIES = 180
MISTRAL_TIMEOUT = 10
MISTRAL_URL = 'http://localhost:8989/v2'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'simple': {
            'format': '%(asctime)s %(levelname)s [%(funcName)s] %(message)s'
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'stdout': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
        },
        'test': {
            'level': 'ERROR',
            'class': 'logging.StreamHandler',
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['test'],
            'level': 'ERROR',
            'propagate': True,
        },
        'pyws': {
            'handlers': ['test'],
            'propagate': True,
            'level': 'ERROR',
        },
        'cornerstone.soap.main': {
            'handlers': ['test'],
            'propagate': True,
            'level': 'ERROR',
        },
    }
}

django.setup()
