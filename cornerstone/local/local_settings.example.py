# Cornerstone configuration
DATABASES = {
    'default': {
        'ENGINE': '',
        'NAME': '',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}
ADMIN_NAME = 'admin_name'
ADMIN_PASS = 'admin_password'  # nosec
ADMIN_PROJECT = 'admin_project'
AUTH_URL = 'http://localhost:5000/v3/'
SERVICE_NAME = 'Cornerstone'
SERVICE_LOCATION = 'http://localhost/api/'
OWNER_ROLE_NAME = 'owner'
KEYSTONE_RETRIES = 100
KEYSTONE_TIMEOUT = 2
GROUP_OPERATORS = 'operator'
ROLE_OPERATORS = 'operator'
GROUP_SUPPORTERS = 'support'
ROLE_SUPPORTERS = 'support'
DOMAINS = [
    'Default'
]
MISTRAL_RETRIES = 180
MISTRAL_TIMEOUT = 10
MISTRAL_URL = 'http://localhost:8989/v2'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'simple': {
            'format': '%(asctime)s %(levelname)s [%(funcName)s] %(message)s'
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'stdout': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
        },
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': '/var/log/cornerstone/cornerstone.log',
            'formatter': 'simple'
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['file'],
            'level': 'ERROR',
            'propagate': True,
        },
        'pyws': {
            'handlers': ['file'],
            'propagate': True,
            'level': 'INFO',
        },
        'cornerstone.soap.main': {
            'handlers': ['file'],
            'propagate': True,
            'level': 'INFO',
        },
    }
}
