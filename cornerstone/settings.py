import logging
import os
import pymysql

# Replace MySQLdb by pymysql
pymysql.version_info = (1, 4, 2, "final", 0)
pymysql.install_as_MySQLdb()

# Django settings for cornerstone project.
DEBUG = False
TEMPLATE_DEBUG = DEBUG

TIME_ZONE = 'Europe/Zurich'
LANGUAGE_CODE = 'en-us'

ROOT_PATH = os.path.dirname(os.path.abspath(__file__))

SITE_ID = 1
USE_I18N = True
USE_L10N = True
USE_TZ = True
MEDIA_ROOT = ''
MEDIA_URL = ''
STATIC_ROOT = '/usr/share/cornerstone/static'
STATIC_URL = '/static/'

STATICFILES_DIRS = ()

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

SECRET_KEY = 'SECRETKEY'  # nosec

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

ROOT_URLCONF = 'cornerstone.urls'

WSGI_APPLICATION = 'cornerstone.wsgi.application'

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.AllowAny',
    ),
}

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'cornerstone'
)

# ALLOWED_HOSTS needed for DEBUG=False in py1.7
# See https://docs.djangoproject.com/en/1.7/ref/settings/#debug
ALLOWED_HOSTS = ['*']

if DEBUG:
    logging.basicConfig(level=logging.DEBUG)
else:
    logging.basicConfig(level=logging.INFO)

# Cornerstone default configuration
DATABASES = {
    'default': {
        'ENGINE': '',
        'NAME': '',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}
ADMIN_NAME = 'admin_name'
ADMIN_PASS = 'admin_password'  # nosec
ADMIN_PROJECT = 'admin_project'
AUTH_URL = 'http://localhost:5000/v3/'
SERVICE_NAME = 'Cornerstone'
SERVICE_LOCATION = 'http://localhost/api/'
OWNER_ROLE_NAME = 'owner'
ADMINISTRATOR_ROLE_NAME = 'coordinator'
KEYSTONE_RETRIES = 100
KEYSTONE_TIMEOUT = 2
GROUP_OPERATORS = 'operator'
ROLE_OPERATORS = 'operator'
GROUP_SUPPORTERS = 'support'
ROLE_SUPPORTERS = 'support'
DOMAINS = [
    'Default'
]
MISTRAL_RETRIES = 180
MISTRAL_TIMEOUT = 10
MISTRAL_URL = 'http://localhost:8989/v2'
COMMAND_MANAGER_DRIVER = 'mistral'

MISTRAL_WORKFLOWS = {
    'service_enable': 'service_enable.{0}',
    'service_disable': 'service_disable.{0}',
    'add_grant_project': 'project_grants.add',
    'delete_grant_project': 'project_grants.revoke',
    'get_project_members': 'project_grants.list',
    'is_allowed': 'project_grants.check',
    'project_create': 'project_create.init',
    'project_delete': 'project_delete.init',
    'project_update': 'project_update.init',
    'project_get': 'project_get.init',
    'project_getinfo': 'project_get.detailed',
    'project_update_property': 'project_property.update',
    'project_list_property': 'project_property.list',
    'service_list': 'service_list.init',
    'project_service_list': 'service_list.project',
    'grant_group_project': 'project_grants.add',
    'revoke_group_project': 'project_grants.revoke',
    'set_project_members': 'project_grants.set',
}

REST_FRAMEWORK = {
    'DEFAULT_PAGINATION_CLASS':
        'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 100
}

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True,
    },
]

try:
    from cornerstone.local.local_settings import *  # noqa: F403,F401, H303
except ImportError:
    logging.warning("No local_settings file found.")
