import logging
import stevedore

from cornerstone import settings

# Get an instance of a logger
LOGGER = logging.getLogger(__name__)


class Manager(object):
    def __init__(self):
        self.driver = stevedore.driver.DriverManager(
            namespace='cornerstone.driver',
            name=settings.COMMAND_MANAGER_DRIVER,
            invoke_on_load=True).driver

    def allow_service(self, service_name, enabled, project_id):
        if enabled:
            return self.driver.service_enable(
                service_name,
                project_id)
        else:
            return self.driver.service_disable(
                service_name,
                project_id)

    def add_grant_project(self, project_id, rolename, members):
        return self.driver.add_grant_project(
            project_id,
            rolename,
            members
        )

    def delete_grant_project(self, project_id, rolename, members):
        return self.driver.delete_grant_project(
            project_id,
            rolename,
            members
        )

    def domain_list(self):
        return self.driver.domain_list()

    def get_project_members(self, project_id, rolename):
        return self.driver.get_project_members(
            project_id,
            rolename
        )

    def is_allowed(self, project_id, group_name, role_name):
        return self.driver.is_allowed(
            project_id,
            group_name,
            role_name)

    def project_create(self, project_id, name, description, owner,
                       administrator='', type='official',
                       status='active', enabled=None):
        return self.driver.project_create(
            project_id=project_id,
            name=name,
            description=description,
            owner=owner,
            administrator=administrator,
            type=type,
            status=status,
            enabled=enabled
        )

    def project_delete(self, project_id):
        return self.driver.project_delete(project_id)

    def project_update(self, project_id, name, description, owner,
                       administrator='', type='', status='', enabled=None):
        return self.driver.project_update(
            project_id=project_id,
            name=name,
            description=description,
            owner=owner,
            administrator=administrator,
            type=type,
            status=status,
            enabled=enabled)

    def project_get(self, project_id):
        return self.driver.project_get(project_id)

    def project_getinfo(self, project_id):
        return self.driver.project_getinfo(project_id)

    def project_list(self, include_type=False):
        return self.driver.project_list(include_type)

    def project_list_groups_grants(self, project_id):
        return self.driver.project_list_groups_grants(project_id)

    def project_add_property(self, project_id, property_name, property_value):
        return self.driver.project_add_property(
            project_id, property_name, property_value
        )

    def project_delete_property(self, project_id, property_name):
        return self.driver.project_delete_property(
            project_id, property_name)

    def project_list_property(self, project_id):
        """List properties for project."""
        return self.driver.project_list_property(project_id)

    def project_update_property(self, project_id, property_name,
                                property_value):
        return self.driver.project_update_property(
            project_id,
            property_name,
            property_value
        )

    def service_list(self):
        return self.driver.service_list()

    def project_service_list(self, project_id):
        return self.driver.project_service_list(project_id)

    def set_allow_group(self, project_id, value, group_name, role_name):
        if value:
            return self.driver.grant_group_project(
                project_id,
                group_name,
                role_name
            )
        else:
            return self.driver.revoke_group_project(
                project_id,
                group_name,
                role_name
            )

    def set_project_members(self, project_id, rolename, members):
        return self.driver.set_project_members(
            project_id,
            rolename,
            members
        )
