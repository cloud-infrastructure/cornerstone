import abc


class DriverManager(object, metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def service_enable(self, service_name, project_id):
        """Allow a service on the project."""

    @abc.abstractmethod
    def service_disable(self, service_name, project_id):
        """Disable a service on the project."""

    @abc.abstractmethod
    def add_grant_project(self, project_id, rolename, members):
        """Grant a role on a member inside a project."""

    @abc.abstractmethod
    def delete_grant_project(self, project_id, rolename, members):
        """Delete a role on a member inside a project."""

    @abc.abstractmethod
    def domain_list(self):
        """List the domains."""

    @abc.abstractmethod
    def get_project_members(self, project_id, rolename):
        """Get the project members."""

    @abc.abstractmethod
    def is_allowed(self, project_id, group_name, role_name):
        """Check if a group has the role in a project."""

    @abc.abstractmethod
    def project_create(self, project_id, name, description, owner,
                       administrator='', type='', status='', enabled=None):
        """Create a project."""

    @abc.abstractmethod
    def project_delete(self, project_id):
        """Delete a project."""

    @abc.abstractmethod
    def project_update(self, project_id, name, description, owner,
                       administrator='', type='', status='', enabled=None):
        """Delete a project."""

    @abc.abstractmethod
    def project_get(self, project_id):
        """Retrieve a dictionary with project and owner info."""

    @abc.abstractmethod
    def project_getinfo(self, project_id):
        """Retrieve detailed information about a project."""

    @abc.abstractmethod
    def project_list(self, include_type=False):
        """List projects."""

    @abc.abstractmethod
    def project_list_groups_grants(self, project_id):
        """Check if operators group and roles are set to."""

    @abc.abstractmethod
    def project_add_property(self, project_id, property_name, property_value):
        """Add new property to project metadata."""

    @abc.abstractmethod
    def project_delete_property(self, project_id, property_name):
        """Delete property from project metadata."""

    @abc.abstractmethod
    def project_list_property(self, project_id):
        """List properties for project."""

    @abc.abstractmethod
    def project_update_property(self, project_id, property_name,
                                property_value):
        """Update property to project."""

    @abc.abstractmethod
    def service_list(self):
        """List services available to users."""

    @abc.abstractmethod
    def project_service_list(self, project_id):
        """List services enabled in a project."""

    @abc.abstractmethod
    def grant_group_project(self, project_id, group_name, role_name):
        """Grant a role of a group in a project."""

    @abc.abstractmethod
    def revoke_group_project(self, project_id, group_name, role_name):
        """Revoke a role of a group in a project."""

    @abc.abstractmethod
    def set_project_members(self, project_id, rolename, members):
        """Set project members."""
