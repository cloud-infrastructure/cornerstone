import logging

from cornerstone.clients.keystone_client import KeystoneClient
from cornerstone.clients.mistral_client import MistralClient
from cornerstone.driver.base import DriverManager
from cornerstone import settings
from cornerstone import utils
from mistralclient.api.base import APIException

# Get an instance of a logger
LOGGER = logging.getLogger(__name__)

KC_ADMIN = KeystoneClient(
    username=settings.ADMIN_NAME,
    password=settings.ADMIN_PASS,
    project_name=settings.ADMIN_PROJECT,
    auth_url=settings.AUTH_URL)

MISTRALCLI = MistralClient(
    session=(KeystoneClient(
        username=settings.ADMIN_NAME,
        password=settings.ADMIN_PASS,
        project_name=settings.ADMIN_PROJECT,
        auth_url=settings.AUTH_URL).session),
    mistral_url=settings.MISTRAL_URL)


class MistralManager(DriverManager):
    def execute_workflow(self, workflow_name, workflow_input, sync=True):
        try:
            workflow = MISTRALCLI.workflow_get(workflow_name)
            LOGGER.info('Execute %s workflow %s',
                        'SYNC' if sync else 'ASYNC', workflow_name)
            description = 'Cornerstone exec %s the workflow %s' % (
                'SYNC' if sync else 'ASYNC',
                workflow_name)
            if sync:
                state, output = MISTRALCLI.execution_create_sync(
                    workflow.id,
                    workflow_input,
                    description,
                    settings.MISTRAL_TIMEOUT,
                    settings.MISTRAL_RETRIES)
                LOGGER.info('Workflow %s execution finished with %s',
                            workflow_name, state)
                if state != 'SUCCESS':
                    return False
                return output or True
            else:
                MISTRALCLI.execution_create_async(workflow.id,
                                                  workflow_input,
                                                  description)
                LOGGER.info('Workflow %s execution triggered', workflow_name)
                return True
        except APIException:
            LOGGER.info('Workflow %s not found skipping...', workflow_name)
            return False

    def get_workflow_name(self, name):
        if name in settings.MISTRAL_WORKFLOWS.keys():
            return settings.MISTRAL_WORKFLOWS[name]
        return None

    def service_enable(self, service_name, project_id):
        wrk_name = self.get_workflow_name('service_enable')
        return self.execute_workflow(
            wrk_name.format(service_name), {'id': project_id})

    def service_disable(self, service_name, project_id):
        wrk_name = self.get_workflow_name('service_disable')
        return self.execute_workflow(
            wrk_name.format(service_name), {'id': project_id})

    def add_grant_project(self, project_id, rolename, members):
        wrk_name = self.get_workflow_name('add_grant_project')
        return self.execute_workflow(wrk_name, {
            'project': project_id,
            'members': members.split(","),
            'role': rolename})

    def delete_grant_project(self, project_id, rolename, members):
        wrk_name = self.get_workflow_name('delete_grant_project')
        return self.execute_workflow(wrk_name, {
            'project': project_id,
            'members': members.split(","),
            'role': rolename})

    def domain_list(self):
        output = []
        for domain_id in settings.DOMAINS:
            output.append({'id': domain_id})
        LOGGER.info("Domains found: %s", len(output))
        return output

    def get_project_members(self, project_id, rolename):
        wrk_name = self.get_workflow_name('get_project_members')
        result = self.execute_workflow(wrk_name, {
            'project': project_id,
            'role': rolename})
        return result['grants'] if result and 'grants' in result else []

    def is_allowed(self, project_id, group_name, role_name):
        wrk_name = self.get_workflow_name('is_allowed')
        result = self.execute_workflow(wrk_name, {
            'project': project_id,
            'member': group_name,
            'role': role_name})
        return result['check'] if result and 'check' in result else False

    def project_create(self, project_id, name, description, owner,
                       administrator='', type='', status='', enabled=None):
        # Remove accents
        name = utils.remove_accents(name)
        description = utils.remove_accents(description)
        if enabled is None and status:
            # If there is no enabled parameter, use status instead
            enabled = True if status == 'active' else False

        wrk_name = self.get_workflow_name('project_create')
        return self.execute_workflow(wrk_name, {
            'id': project_id,
            'name': name,
            'description': description,
            'enabled': enabled,
            'status': status,
            'type': type,
            'owner': owner,
            'administrator': administrator
        })

    def project_delete(self, project_id):
        wrk_name = self.get_workflow_name('project_delete')
        return self.execute_workflow(wrk_name, {'id': project_id})

    def project_update(self, project_id, name=None, description=None,
                       owner=None, administrator='', type='', status='',
                       enabled=None):
        # Remove accents
        if name:
            name = utils.remove_accents(name)
        if description:
            description = utils.remove_accents(description)
        if enabled is None and status:
            # If there is no enabled parameter, use status instead
            enabled = True if status == 'active' else False

        wrk_name = self.get_workflow_name('project_update')
        return self.execute_workflow(wrk_name, {
            'id': project_id,
            'name': name,
            'description': description,
            'enabled': enabled,
            'status': status,
            'type': type,
            'owner': owner,
            'administrator': administrator})

    def project_get(self, project_id):
        """Retrieve a dictionary with project and owner info.

        :param project_id: Id of the project to obtain info
        :returns: dict()
        :raises: ClientException if keystoneclient can not connect.
        :raises: NotFound if project have not been found
        """
        wrk_name = self.get_workflow_name('project_get')
        return self.execute_workflow(wrk_name, {
            'id': project_id})

    def project_getinfo(self, project_id):
        wrk_name = self.get_workflow_name('project_getinfo')
        return self.execute_workflow(wrk_name, {
            'id': project_id})

    def project_list(self, include_type=False):
        # TODO(jcastro) replace the old code by a workflow call
        # At the moment the execution time is too long for getting the response
        # on an appropiate time.
        # result = self.execute_workflow('project_get.all', {})
        # return result['projects'] if 'projects' in result else []
        projects = KC_ADMIN.list_project()
        owner_role = KC_ADMIN.get_role_by_name(
            settings.OWNER_ROLE_NAME)
        administrator_role = KC_ADMIN.get_role_by_name(
            settings.ADMINISTRATOR_ROLE_NAME)

        # we need only assignments with project in the scope
        output = []
        assignments_owner = KC_ADMIN.list_role_assignments(
            role=owner_role.id)
        assignments_admins = KC_ADMIN.list_role_assignments(
            role=administrator_role.id)
        for project in projects:
            project.owner = ''
            for assig in assignments_owner:
                if ('project' in assig.scope
                        and project.id == assig.scope['project']['id']
                        and hasattr(assig, 'user')):
                    project.owner = assig.user['id']
            project.administrator = ''
            for assig in assignments_admins:
                if ('project' in assig.scope
                        and project.id == assig.scope['project']['id']
                        and hasattr(assig, 'group')):
                    project.administrator = assig.group['id']
            output.append(project.__dict__)
        return output

    def project_list_groups_grants(self, project_id):
        """Check if operators group and roles are set to.

        project. Returns a list with groups allowed
        :returns: list({'group_name': 'operators'}, ...)
        """
        grant_list = []
        # Check if operators are allowed in the project
        if self.is_allowed(project_id,
                           settings.GROUP_OPERATORS,
                           settings.ROLE_OPERATORS):
            grant_list.append({"group_name": "operators"})
        # Check if supporters are allowed in the project
        if self.is_allowed(project_id,
                           settings.GROUP_SUPPORTERS,
                           settings.ROLE_SUPPORTERS):
            grant_list.append({"group_name": "supporters"})
        return grant_list

    def project_add_property(self, project_id, property_name, property_value):
        """Add new property to project metadata.

        :returns: True if it works
        """
        wrk_name = self.get_workflow_name('project_update_property')
        return self.execute_workflow(wrk_name, {
            'project': project_id,
            'properties': {
                property_name: property_value}})

    def project_delete_property(self, project_id, property_name):
        """Delete property from project metadata."""
        wrk_name = self.get_workflow_name('project_update_property')
        return self.execute_workflow(wrk_name, {
            'project': project_id,
            'properties': {
                property_name: ""}})

    def project_list_property(self, project_id):
        """List properties for project."""
        wrk_name = self.get_workflow_name('project_list_property')
        result = self.execute_workflow(wrk_name, {
            'project': project_id})
        return result['properties'] if (
            result and 'properties') in result else []

    def project_update_property(self, project_id, property_name,
                                property_value):
        """Update property to project."""
        wrk_name = self.get_workflow_name('project_update_property')
        return self.execute_workflow(wrk_name, {
            'project': project_id,
            'properties': {
                property_name: property_value}})

    def service_list(self):
        wrk_name = self.get_workflow_name('service_list')
        result = self.execute_workflow(wrk_name, {})
        return result['services'] if 'services' in result else []

    def project_service_list(self, project_id):
        wrk_name = self.get_workflow_name('project_service_list')
        result = self.execute_workflow(wrk_name, {
            'project': project_id})
        return result['services'] if 'services' in result else []

    def grant_group_project(self, project_id, group_name, role_name):
        wrk_name = self.get_workflow_name('grant_group_project')
        return self.execute_workflow(wrk_name, {
            'project': project_id,
            'members': [group_name],
            'role': role_name})

    def revoke_group_project(self, project_id, group_name, role_name):
        wrk_name = self.get_workflow_name('revoke_group_project')
        return self.execute_workflow(wrk_name, {
            'project': project_id,
            'members': [group_name],
            'role': role_name})

    def set_project_members(self, project_id, rolename, members):
        wrk_name = self.get_workflow_name('set_project_members')
        return self.execute_workflow(wrk_name, {
            'project': project_id,
            'members': members.split(","),
            'role': rolename})
