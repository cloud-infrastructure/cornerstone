from cornerstone.rest.v2.models import project
from rest_framework import serializers


class ProjectSerializer(serializers.ModelSerializer):
    id = serializers.CharField(max_length=255, required=True)
    name = serializers.CharField(max_length=255, required=True)
    description = serializers.CharField(max_length=255, default='',
                                        allow_blank=True, required=False)
    type = serializers.CharField(max_length=255, required=True)
    status = serializers.CharField(max_length=255, required=True)
    owner = serializers.CharField(max_length=255, required=True)
    administrator = serializers.CharField(max_length=255, default=None,
                                          allow_null=True, required=False)

    class Meta(object):
        model = project
        fields = ('id', 'name', 'description', 'type', 'status', 'owner',
                  'administrator')


class ProjectUpdateSerializer(serializers.ModelSerializer):
    id = serializers.CharField(max_length=255, required=False)
    name = serializers.CharField(max_length=255, required=False)
    description = serializers.CharField(max_length=255, required=False)
    type = serializers.CharField(max_length=255, required=False)
    status = serializers.CharField(max_length=255, required=False)
    owner = serializers.CharField(max_length=255, required=False)
    administrator = serializers.CharField(max_length=255, required=False)

    class Meta(object):
        model = project
        fields = ('id', 'name', 'description', 'type', 'status', 'owner',
                  'administrator')
