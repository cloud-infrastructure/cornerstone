import logging

from django.apps import apps
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView

from cornerstone.manager import Manager
from cornerstone.rest.v2.serializers import ProjectSerializer
from cornerstone.rest.v2.serializers import ProjectUpdateSerializer


# Get an instance of a logger
LOGGER = logging.getLogger(__name__)


class APIViewCommandServer(APIView):
    cmdServer = Manager()


class ProjectList(APIViewCommandServer):
    """List all projects, or create a new project."""

    def get(self, request, format=None):
        LOGGER.info('List projects')
        projects = self.cmdServer.project_list(include_type=True)
        serializer = ProjectSerializer(projects, many=True)
        return Response(data={
            'pagination': {},
            'data': serializer.data
        })

    def post(self, request, format=None):
        LOGGER.info('Create project')
        serializer = ProjectSerializer(data=request.data)
        if serializer.is_valid():
            try:
                project = self.cmdServer.project_create(
                    project_id=serializer.data['id'],
                    name=serializer.data['name'],
                    description=serializer.data['description'],
                    type=serializer.data['type'],
                    status=serializer.data['status'],
                    owner=serializer.data['owner'],
                    administrator=serializer.data['administrator'],)
                if not project:
                    error_msg = 'Project create failed on "{0}"'.format(
                        serializer.data['id'])
                    LOGGER.error(error_msg)
                    return Response(
                        {'detail': error_msg},
                        status=status.HTTP_412_PRECONDITION_FAILED)

                LOGGER.info('Project created with id "%s"',
                            serializer.data['id'])
                return Response(
                    data={'data': serializer.data},
                    status=status.HTTP_201_CREATED)
            except Exception as exception:
                LOGGER.exception(str(exception))
                return Response({'detail': str(exception)},
                                status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        LOGGER.info("Validation errors: %s", serializer.errors)
        LOGGER.info('Project creation cancelled')
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProjectDetail(APIViewCommandServer):
    """Retrieve, update or delete a project instance."""

    def get(self, request, pk, format=None):
        try:
            LOGGER.info("Get project info '%s'", pk)
            project = self.cmdServer.project_get(pk)
            if not project:
                error_msg = 'Project get failed on "{0}"'.format(pk)
                LOGGER.error(error_msg)
                return Response(
                    {'detail': error_msg},
                    status=status.HTTP_412_PRECONDITION_FAILED)

            serializer = ProjectSerializer(project)
            return Response(data={'data': serializer.data})
        except Exception as exception:
            LOGGER.exception(str(exception))
            return Response({'detail': str(exception)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def put(self, request, pk, format=None):
        LOGGER.info("Update project '%s'", pk)

        serializer = ProjectUpdateSerializer(data=request.data)
        if serializer.is_valid():
            try:
                name = (serializer.data['name']
                        if 'name' in serializer.data else None)
                description = (serializer.data['description']
                               if 'description' in serializer.data else None)
                type = (serializer.data['type']
                        if 'type' in serializer.data else None)
                project_status = (
                    serializer.data['status']
                    if 'status' in serializer.data else None)
                owner = (serializer.data['owner']
                         if 'owner' in serializer.data else None)
                administrator = (
                    serializer.data['administrator']
                    if 'administrator' in serializer.data else None)

                project = self.cmdServer.project_update(
                    project_id=pk,
                    name=name,
                    description=description,
                    type=type,
                    status=project_status,
                    owner=owner,
                    administrator=administrator)
                if not project:
                    error_msg = 'Project update failed on "{0}"'.format(pk)
                    LOGGER.error(error_msg)
                    return Response(
                        {'detail': error_msg},
                        status=status.HTTP_412_PRECONDITION_FAILED)

                LOGGER.info("Project '%s' updated", pk)
                serializer = ProjectSerializer(project)
                return Response(data={'data': serializer.data})
            except Exception as exception:
                LOGGER.info(str(exception))
                return Response({'detail': str(exception)},
                                status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        LOGGER.info(serializer.errors)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        LOGGER.info("Delete project '%s'", pk)
        # Delete it and related services
        try:
            output = self.cmdServer.project_delete(pk)
            if not output:
                error_msg = 'Project delete failed on "{0}"'.format(pk)
                LOGGER.error(error_msg)
                return Response({'detail': error_msg},
                                status=status.HTTP_412_PRECONDITION_FAILED)

            LOGGER.info("Project '%s' deleted", pk)
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Exception as exception:
            # Exception raised in case the project is locked
            LOGGER.info(str(exception))
            return Response({'detail': str(exception)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class SchemaDetail(APIView):

    # This is the mapping between django field types and FIM field types
    attr_type_mapping = {
        # django_type: fim_type
        'CharField': 'String',
        'BooleanField': 'Boolean',
        'IntegerField': 'Integer',
    }

    def _get_schema(self):
        schema = []
        schemas_to_return = ['project']
        for m in apps.get_models():
            # We want only person and project models information
            if m.__name__ in schemas_to_return:
                d = {"name": m.__name__,
                     "properties": []}
                for fname in m._meta.get_fields():
                    ftype = fname.get_internal_type()
                    # AutoField are not needed (like aout id field)
                    if ftype == 'AutoField':
                        continue
                    attr_type = self.attr_type_mapping[ftype]
                    dict_attrs = {"name": fname.name,
                                  "property_type": attr_type}
                    if fname.primary_key:
                        dict_attrs['id'] = True
                    d["properties"].append(dict_attrs)
                schema.append(d)
        return schema

    def get(self, request, format=None):
        LOGGER.info('List schemas')
        return Response(self._get_schema())
