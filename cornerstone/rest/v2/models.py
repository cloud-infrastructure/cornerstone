from django.db import models


class project(models.Model):
    class status(models.TextChoices):
        ACTIVE = "active"
        BLOCKED = "blocked"
        ARCHIVED = "archived"

    id = models.CharField(max_length=255, primary_key=True)
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
    type = models.CharField(max_length=255)
    status = models.CharField(max_length=255,
                              choices=status)
    owner = models.CharField(max_length=255)
    administrator = models.CharField(max_length=255)
