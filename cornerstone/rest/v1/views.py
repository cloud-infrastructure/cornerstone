import logging

from django.apps import apps
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView

from cornerstone.manager import Manager
from cornerstone.rest.v1.models import PersonV1 as Person
from cornerstone.rest.v1.serializers import AssignmentInputSerializer
from cornerstone.rest.v1.serializers import AssignmentSerializer
from cornerstone.rest.v1.serializers import DomainSerializer
from cornerstone.rest.v1.serializers import GrantSerializer
from cornerstone.rest.v1.serializers import PersonSerializer
from cornerstone.rest.v1.serializers import ProjectListSerializer
from cornerstone.rest.v1.serializers import ProjectSerializer
from cornerstone.rest.v1.serializers import ProjectUpdateSerializer
from cornerstone.rest.v1.serializers import PropertySerializer
from cornerstone.rest.v1.serializers import ServiceAssignmentSerializer
from cornerstone.rest.v1.serializers import ServiceSerializer
from cornerstone import settings

# Get an instance of a logger
LOGGER = logging.getLogger(__name__)


class APIViewCommandServer(APIView):
    cmdServer = Manager()


class AssignmentList(APIViewCommandServer):
    """List assignments by project_id and role name.

    It also creates new assignments.
    """

    def _get_members_string_from_list(self, members):
        # Cornerstone core needs members as string comman separated
        member_list = []
        if isinstance(members, list):
            for member_dict in members:
                member_list.append(member_dict['name'])
        else:
            member_list.append(members['name'])
        if not member_list:
            raise Exception("Members not found on request")
        return ",".join(member_list)

    def get(self, request, project_id, rolename, format=None):
        LOGGER.info("Get members of project '%s' with role '%s'",
                    project_id, rolename)
        try:
            members = self.cmdServer.get_project_members(project_id, rolename)
            if not members:
                message = 'Members not found with role name' \
                          ' "{0}" in project "{1}"'.format(rolename,
                                                           project_id)
                LOGGER.warning(message)
                return Response({'detail': message},
                                status=status.HTTP_404_NOT_FOUND)

            serializer = AssignmentSerializer(members, many=True)
            return Response(serializer.data)
        except Exception as exception:
            LOGGER.exception(str(exception))
            return Response({'detail': str(exception)},
                            status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, project_id, rolename, format=None):
        LOGGER.info("Add members to project '%s' with role '%s'",
                    project_id, rolename)
        serializer = AssignmentInputSerializer(data=request.data)
        if serializer.is_valid():
            try:
                members = self._get_members_string_from_list(
                    serializer.data['members'])
                LOGGER.info("Adding members %s from project %s "
                            "with role %s", members, project_id, rolename)
                ret = self.cmdServer.add_grant_project(project_id,
                                                       rolename,
                                                       members)
                if not ret:
                    error_msg = 'There was an error adding an assignment'
                    LOGGER.error(error_msg)
                    return Response(
                        {'detail': error_msg},
                        status=status.HTTP_412_PRECONDITION_FAILED)

                LOGGER.info('Members "%s" added from project "%s" '
                            'and rolename "%s"',
                            members, project_id, rolename)
                return Response(serializer.data,
                                status=status.HTTP_201_CREATED)
            except Exception as exception:
                LOGGER.exception(str(exception))
                return Response({'detail': str(exception)},
                                status=status.HTTP_400_BAD_REQUEST)

        LOGGER.info("Validation errors: %s", serializer.errors)
        LOGGER.info("Add project members cancelled")
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, project_id, rolename, format=None):
        LOGGER.info('Set/update project members')
        serializer = AssignmentInputSerializer(data=request.data)
        if serializer.is_valid():
            try:
                members = self._get_members_string_from_list(
                    serializer.data['members'])
                LOGGER.info("Updating members %s from project %s "
                            "with role %s", members, project_id, rolename)
                ret = self.cmdServer.set_project_members(project_id,
                                                         rolename,
                                                         members)
                if not ret:
                    error_msg = 'There was an error setting an assignment'
                    LOGGER.error(error_msg)
                    return Response(
                        {'detail': error_msg},
                        status=status.HTTP_412_PRECONDITION_FAILED)
                LOGGER.info('Members "%s" updated from project "%s" '
                            'and rolename "%s"',
                            members, project_id, rolename)
                return Response(status=status.HTTP_204_NO_CONTENT)
            except Exception as exception:
                LOGGER.exception(str(exception))
                return Response({'detail': str(exception)},
                                status=status.HTTP_400_BAD_REQUEST)

        LOGGER.info("Validation errors: %s", serializer.errors)
        LOGGER.info("Set/update project members cancelled")
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, project_id, rolename,
               membername=None, format=None):
        LOGGER.info('Delete project members')
        serializer = AssignmentInputSerializer(data=request.data)
        # Cornerstone core needs members as string comma separated list
        if membername:
            members = membername
        else:
            members = self._get_members_string_from_list(
                request.data['members'])

        if not project_id or not rolename:
            LOGGER.info("Validation errors: %s", serializer.errors)
            LOGGER.info("Delete project members cancelled")
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)

        LOGGER.info("Delete members %s from project %s with role %s",
                    members, project_id, rolename)
        try:
            ret = self.cmdServer.delete_grant_project(project_id,
                                                      rolename,
                                                      members)
            if not ret:
                error_msg = 'There was an error removing an assignment'
                LOGGER.error(error_msg)
                return Response(
                    {'detail': error_msg},
                    status=status.HTTP_412_PRECONDITION_FAILED)

            LOGGER.info('Members "%s" deleted from project "%s" '
                        'and rolename "%s"',
                        members, project_id, rolename)
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Exception as exception:
            LOGGER.exception(str(exception))
            return Response({'detail': str(exception)},
                            status=status.HTTP_400_BAD_REQUEST)


class DomainList(APIViewCommandServer):
    """List all domains."""

    def get(self, request, format=None):
        LOGGER.info("List domains")
        try:
            domains = self.cmdServer.domain_list()
            serializer = DomainSerializer(domains, many=True)
            return Response(serializer.data)
        except IOError as exception:
            error_msg = exception.strerror + ": " + exception.filename
            LOGGER.exception(error_msg)
            return Response({'detail': error_msg},
                            status=status.HTTP_400_BAD_REQUEST)


class GrantList(APIViewCommandServer):

    # The values here are coming from server.conf
    gr_rel = {
        'operators': {
            'group_name': settings.GROUP_OPERATORS,
            'role_name': settings.ROLE_OPERATORS
        },
        'supporters': {
            'group_name': settings.GROUP_SUPPORTERS,
            'role_name': settings.ROLE_SUPPORTERS
        }
    }

    def get(self, request, project_id, format=None):
        LOGGER.info('List grants for project "%s"', project_id)
        grant_list = self.cmdServer.project_list_groups_grants(project_id)
        serializer = GrantSerializer(grant_list, many=True)
        return Response(serializer.data)

    def delete(self, request, project_id, groupname=None, format=None):
        LOGGER.info('Delete grant for project "%s"', project_id)
        if not groupname:
            groupname = request.data['group_name']
        return self.postdelete(False, project_id, groupname, request, format)

    def post(self, request, project_id, format=None):
        LOGGER.info('Add grant for project "%s"', project_id)
        groupname = None
        if 'group_name' in request.data:
            groupname = request.data['group_name']
        return self.postdelete(True, project_id, groupname, request, format)

    def postdelete(self, enabled, project_id, groupname, request, format=None):
        serializer = GrantSerializer(data={"group_name": groupname})
        if serializer.is_valid():
            try:
                # We will translate the grant to configured
                # group and role
                LOGGER.info('Setting "%s" group "%s" with role "%s" '
                            'to project "%s"',
                            self.gr_rel[groupname]['group_name'],
                            self.gr_rel[groupname]['role_name'],
                            project_id,
                            enabled)
                ret = self.cmdServer.set_allow_group(
                    project_id=project_id,
                    value=enabled,
                    group_name=self.gr_rel[groupname]['group_name'],
                    role_name=self.gr_rel[groupname]['role_name'])

                if not ret:
                    error_msg = 'There was an error adding/deleting a grant'
                    LOGGER.error(error_msg)
                    return Response(
                        {'detail': error_msg},
                        status=status.HTTP_412_PRECONDITION_FAILED)

                stat = status.HTTP_201_CREATED
                if not enabled:
                    stat = status.HTTP_204_NO_CONTENT

                LOGGER.info('Group "%s" with role "%s" set "%s" '
                            'to project "%s"',
                            self.gr_rel[groupname]['group_name'],
                            self.gr_rel[groupname]['role_name'],
                            project_id,
                            enabled)
                return Response(serializer.data, status=stat)
            except Exception as exception:
                LOGGER.exception(exception)
                return Response({'detail': str(exception)},
                                status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        LOGGER.info("Validation errors: %s", serializer.errors)
        LOGGER.info('Allow operators cancelled')
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PersonList(APIView):
    """List all persons, or create a new person."""

    def get(self, request, format=None):
        persons = Person.objects.all()
        serializer = PersonSerializer(persons, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = PersonSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PersonDetail(APIView):
    """Retrieve, update or delete a person instance."""

    def get_object(self, pk):
        try:
            return Person.objects.get(pk=pk)
        except ObjectDoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        person = self.get_object(pk)
        serializer = PersonSerializer(person)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        person = self.get_object(pk)
        serializer = PersonSerializer(person, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        person = self.get_object(pk)
        person.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class PropertiesList(APIViewCommandServer):
    """List all properties, or add new proeprty to project."""

    def get(self, request, pk, format=None):
        LOGGER.info('List project properties')
        properties = self.cmdServer.project_list_property(pk)
        serializer = PropertySerializer(properties, many=True)
        return Response(serializer.data)

    def post(self, request, pk, format=None):
        LOGGER.info('Add new property to project "%s"', pk)
        serializer = PropertySerializer(data=request.data)
        if serializer.is_valid():
            LOGGER.info('Property to add: "%s"="%s"',
                        serializer.data['property_name'],
                        serializer.data['property_value'])
            try:
                ret = self.cmdServer.project_add_property(
                    pk,
                    serializer.data['property_name'],
                    serializer.data['property_value'])
                if not ret:
                    error_msg = 'There was an error adding a property'
                    LOGGER.error(error_msg)
                    return Response(
                        {'detail': error_msg},
                        status=status.HTTP_412_PRECONDITION_FAILED)

                LOGGER.info(
                    'Property "%s" with value "%s" added to project "%s"',
                    serializer.data['property_name'],
                    serializer.data['property_value'],
                    pk)
                return Response(serializer.data,
                                status=status.HTTP_201_CREATED)
            except ValueError as exception:
                LOGGER.exception(str(exception))
                return Response({'detail': str(exception)},
                                status=status.HTTP_400_BAD_REQUEST)
            except Exception as exception:
                LOGGER.exception(exception)
                return Response({'detail': str(exception)},
                                status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        LOGGER.info("Validation errors: %s", serializer.errors)
        LOGGER.info('Property addition cancelled')
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PropertiesDetail(APIViewCommandServer):
    def put(self, request, pk, property_name, format=None):
        LOGGER.info("Update property '%s' in project '%s'", property_name, pk)

        serializer = PropertySerializer(data=request.data)
        if serializer.is_valid():
            try:
                ret = self.cmdServer.project_update_property(
                    pk,
                    serializer.data['property_name'],
                    serializer.data['property_value'])
                if not ret:
                    error_msg = 'There was an error updating a property'
                    LOGGER.error(error_msg)
                    return Response(
                        {'detail': error_msg},
                        status=status.HTTP_412_PRECONDITION_FAILED)

                LOGGER.info('Property "%s" with value "%s" '
                            'updated in project "%s"',
                            serializer.data['property_name'],
                            serializer.data['property_value'],
                            pk)
                return Response(serializer.data)
            except Exception as exception:
                LOGGER.exception(exception)
                return Response({'detail': str(exception)},
                                status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        LOGGER.info("Validation errors: %s", serializer.errors)
        LOGGER.info('Property update cancelled')
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, property_name, format=None):
        LOGGER.info("Delete property '%s' in project '%s'", property_name, pk)

        try:
            ret = self.cmdServer.project_delete_property(pk, property_name)
            if not ret:
                error_msg = 'There was an error deleting a property'
                LOGGER.error(error_msg)
                return Response(
                    {'detail': error_msg},
                    status=status.HTTP_412_PRECONDITION_FAILED)

            LOGGER.info("Property '%s' removed from project '%s'",
                        property_name, pk)
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Exception as exception:
            LOGGER.exception(exception)
            return Response({'detail': str(exception)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class ProjectList(APIViewCommandServer):
    """List all projects, or create a new project."""

    def get(self, request, format=None):
        LOGGER.info('List projects')
        projects = self.cmdServer.project_list()
        serializer = ProjectListSerializer(projects, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        LOGGER.info('Create project')
        serializer = ProjectSerializer(data=request.data)
        if serializer.is_valid():
            try:
                project = self.cmdServer.project_create(
                    project_id=serializer.data['id'],
                    name=serializer.data['name'],
                    description=serializer.data['description'],
                    enabled=serializer.data['enabled'],
                    owner=serializer.data['owner'])
                if not project:
                    error_msg = 'Project create failed on "{0}"'.format(
                        serializer.data['id'])
                    LOGGER.error(error_msg)
                    return Response(
                        {'detail': error_msg},
                        status=status.HTTP_412_PRECONDITION_FAILED)

                LOGGER.info('Project created with id "%s"',
                            serializer.data['id'])
                return Response(serializer.data,
                                status=status.HTTP_201_CREATED)
            except Exception as exception:
                LOGGER.exception(str(exception))
                return Response({'detail': str(exception)},
                                status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        LOGGER.info("Validation errors: %s", serializer.errors)
        LOGGER.info('Project creation cancelled')
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProjectDetail(APIViewCommandServer):
    """Retrieve, update or delete a project instance."""

    def get(self, request, pk, format=None):
        try:
            LOGGER.info("Get project info '%s'", pk)
            project = self.cmdServer.project_getinfo(pk)
            if not project:
                error_msg = 'Project get failed on "{0}"'.format(pk)
                LOGGER.error(error_msg)
                return Response(
                    {'detail': error_msg},
                    status=status.HTTP_412_PRECONDITION_FAILED)

            serializer = ProjectSerializer(project)
            return Response(serializer.data)
        except Exception as exception:
            LOGGER.exception(str(exception))
            return Response({'detail': str(exception)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def put(self, request, pk, format=None):
        LOGGER.info("Update project '%s'", pk)

        serializer = ProjectUpdateSerializer(data=request.data)
        if serializer.is_valid():
            try:
                name = (serializer.data['name']
                        if 'name' in serializer.data else None)
                description = (serializer.data['description']
                               if 'description' in serializer.data else None)
                enabled = (serializer.data['enabled']
                           if 'enabled' in serializer.data else None)
                owner = (serializer.data['owner']
                         if 'owner' in serializer.data else None)

                project = self.cmdServer.project_update(
                    project_id=pk,
                    name=name,
                    description=description,
                    enabled=enabled,
                    owner=owner)
                if not project:
                    error_msg = 'Project update failed on "{0}"'.format(pk)
                    LOGGER.error(error_msg)
                    return Response(
                        {'detail': error_msg},
                        status=status.HTTP_412_PRECONDITION_FAILED)

                LOGGER.info("Project '%s' updated", pk)
                serializer = ProjectSerializer(project)
                return Response(serializer.data)
            except Exception as exception:
                LOGGER.info(str(exception))
                return Response({'detail': str(exception)},
                                status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        LOGGER.info(serializer.errors)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        LOGGER.info("Delete project '%s'", pk)
        # Delete it and related services
        try:
            output = self.cmdServer.project_delete(pk)
            if not output:
                error_msg = 'Project delete failed on "{0}"'.format(pk)
                LOGGER.error(error_msg)
                return Response({'detail': error_msg},
                                status=status.HTTP_412_PRECONDITION_FAILED)

            LOGGER.info("Project '%s' deleted", pk)
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Exception as exception:
            # Exception raised in case the project is locked
            LOGGER.info(str(exception))
            return Response({'detail': str(exception)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class ServiceList(APIViewCommandServer):
    """List all services."""

    def get(self, request, format=None):
        LOGGER.info("List all services")
        try:
            services = self.cmdServer.service_list()
            serializer = ServiceSerializer(services, many=True)
            return Response(serializer.data)
        except IOError as exception:
            LOGGER.exception("%s:%s", exception.strerror, exception.filename)
            error_msg = exception.strerror + ": " + exception.filename
            return Response({'detail': error_msg},
                            status=status.HTTP_400_BAD_REQUEST)


class ServiceAssignmentList(APIViewCommandServer):

    def get(self, request, pk, format=None):
        LOGGER.info('List services for projects')
        services = self.cmdServer.project_service_list(pk)
        serializer = ServiceAssignmentSerializer(services, many=True)
        return Response(serializer.data)

    def postdelete(self, enabled, project_id, servicename, request,
                   format=None):
        valid = True
        if not servicename:
            serializer = ServiceAssignmentSerializer(data=request.data)
            valid = serializer.is_valid()
        if not project_id or not valid:
            LOGGER.info("Validation errors: %s", serializer.errors)
            LOGGER.info('Allow service cancelled')
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)
        if not servicename:
            servicename = serializer.data['service_name']
        try:
            LOGGER.info('Allow "%s" for service "%s" in project "%s',
                        enabled, servicename, project_id)

            ret = self.cmdServer.allow_service(
                servicename, enabled, project_id)

            if not ret:
                warn_msg = "Service allow/disallow failed"
                LOGGER.warning(warn_msg)
                return Response({'detail': warn_msg},
                                status=status.HTTP_412_PRECONDITION_FAILED)
            LOGGER.info('Service "%s" for project "%s" allowed "%s',
                        servicename, project_id, enabled)

            stat = status.HTTP_201_CREATED
            if not enabled:
                stat = status.HTTP_204_NO_CONTENT
            return Response(request.data, status=stat)
        except Exception as exception:
            LOGGER.exception(str(exception))
            return Response({'detail': str(exception)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def delete(self, request, pk, servicename=None, format=None):
        return self.postdelete(False, pk, servicename, request, format)

    def post(self, request, pk, servicename=None, format=None):
        return self.postdelete(True, pk, servicename, request, format)


class SchemaDetail(APIView):

    # This is the mapping between django field types and FIM field types
    attr_type_mapping = {
        # django_type: fim_type
        'CharField': 'String',
        'BooleanField': 'Boolean',
        'IntegerField': 'Integer',
    }

    def _get_schema(self):
        schema = []
        schemas_to_return = ['Person', 'Project']
        for m in apps.get_models():
            # We want only person and project models information
            if m.__name__ in schemas_to_return:
                d = {"name": m.__name__,
                     "properties": []}
                for fname in m._meta.get_fields():
                    ftype = fname.get_internal_type()
                    # AutoField are not needed (like aout id field)
                    if ftype == 'AutoField':
                        continue
                    attr_type = self.attr_type_mapping[ftype]
                    dict_attrs = {"name": fname.name,
                                  "property_type": attr_type}
                    if fname.primary_key:
                        dict_attrs['id'] = True
                    d["properties"].append(dict_attrs)
                schema.append(d)
        return schema

    def get(self, request, format=None):
        LOGGER.info('List schemas')
        return Response(self._get_schema())
