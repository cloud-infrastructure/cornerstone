from django.db import models


class Assignment(models.Model):
    name = models.CharField(max_length=255)
    project_id = models.CharField(max_length=255)
    rolename = models.CharField(max_length=255)


class Domain(models.Model):
    id = models.CharField(max_length=255, primary_key=True)


class Grant(models.Model):
    project_id = models.CharField(max_length=255)
    group_name = models.CharField(max_length=255)


class PersonV1(models.Model):
    id = models.CharField(max_length=255, primary_key=True)
    login = models.CharField(max_length=255)


class ProjectV1(models.Model):
    id = models.CharField(max_length=255, primary_key=True)
    name = models.CharField(max_length=255)
    owner = models.CharField(max_length=255)
    enabled = models.BooleanField(default=True)
    description = models.CharField(max_length=255)


class Property(models.Model):
    property_name = models.CharField(max_length=255, primary_key=True)
    property_value = models.CharField(max_length=255)


class Service(models.Model):
    id = models.CharField(max_length=255, primary_key=True)
    description = models.CharField(max_length=255)


class ServiceAssignment(models.Model):
    project_id = models.CharField(max_length=255)
    service_name = models.CharField(max_length=255)
