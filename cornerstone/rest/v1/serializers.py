from cornerstone.rest.v1.models import Assignment
from cornerstone.rest.v1.models import Domain
from cornerstone.rest.v1.models import Grant
from cornerstone.rest.v1.models import PersonV1 as Person
from cornerstone.rest.v1.models import ProjectV1 as Project
from cornerstone.rest.v1.models import Property
from cornerstone.rest.v1.models import Service
from cornerstone.rest.v1.models import ServiceAssignment
from rest_framework import serializers


class AssignmentSerializer(serializers.ModelSerializer):
    """This serializer is only for GET requests."""

    class Meta(object):
        model = Assignment
        fields = ('name',)


class AssignmentInputSerializer(serializers.ModelSerializer):
    """This serializer is only for POST requests."""

    # This field is removed from model to be able to manage ListField
    members = serializers.ListField(child=serializers.DictField())

    class Meta(object):
        model = Assignment
        fields = ('members',)


class DomainSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = Domain
        fields = ('id',)


class GrantSerializer(serializers.ModelSerializer):

    group_name = serializers.ChoiceField(["operators", "supporters"])

    class Meta(object):
        model = Grant
        fields = ('group_name',)


class PersonSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = Person
        fields = ('id', 'login')


class ProjectSerializer(serializers.ModelSerializer):
    id = serializers.CharField(max_length=255, required=True)
    name = serializers.CharField(max_length=255, required=True)
    owner = serializers.CharField(max_length=255, required=True)
    enabled = serializers.BooleanField(default=True, required=False)
    description = serializers.CharField(max_length=255, default=None,
                                        required=False)
    allowOperators = serializers.BooleanField(default=False, required=False)
    allowSupporters = serializers.BooleanField(default=False, required=False)
    # This field is removed from model to be able to manage ListField
    # as optional field
    services = serializers.ListField(
        child=serializers.CharField(),
        required=False
    )

    class Meta(object):
        model = Project
        fields = ('id', 'name', 'owner', 'enabled', 'description',
                  'allowOperators', 'allowSupporters', 'services')


class ProjectListSerializer(serializers.ModelSerializer):
    id = serializers.CharField(max_length=255, required=True)
    name = serializers.CharField(max_length=255, required=True)
    owner = serializers.CharField(max_length=255, required=True)
    enabled = serializers.BooleanField(default=True, required=False)
    description = serializers.CharField(max_length=255, default=None,
                                        required=False)

    class Meta(object):
        model = Project
        fields = ('id', 'name', 'owner', 'enabled', 'description')


class ProjectUpdateSerializer(serializers.ModelSerializer):

    id = serializers.CharField(max_length=255, required=False)
    name = serializers.CharField(max_length=255, required=False)
    owner = serializers.CharField(max_length=255, required=False)
    enabled = serializers.BooleanField(default=True, required=False)
    description = serializers.CharField(max_length=255, default=None,
                                        required=False)
    allowOperators = serializers.BooleanField(default=False, required=False)
    allowSupporters = serializers.BooleanField(default=False, required=False)
    services = serializers.ListField(
        child=serializers.CharField(),
        required=False
    )

    class Meta(object):
        model = Project
        fields = ('id', 'name', 'owner', 'enabled', 'description',
                  'allowOperators', 'allowSupporters', 'services')


class PropertySerializer(serializers.ModelSerializer):
    property_name = serializers.CharField(max_length=255)
    property_value = serializers.CharField(max_length=255)

    class Meta(object):
        model = Property
        fields = ('property_name', 'property_value')


class ServiceSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = Service
        fields = ('id', 'description')


class ServiceAssignmentSerializer(serializers.ModelSerializer):
    project_id = serializers.CharField(max_length=255, required=False)
    service_name = serializers.CharField(max_length=255)

    class Meta(object):
        model = ServiceAssignment
        fields = ('project_id', 'service_name')
