from django.conf import settings
from django.conf.urls import re_path
from django.conf.urls.static import static
from pyws.adapters._django import serve
from rest_framework.urlpatterns import format_suffix_patterns

from cornerstone.rest.v1 import views as views_v1
from cornerstone.rest.v2 import views as views_v2
from cornerstone.soap.main import SERVER

urlpatterns = [
    # SOAP interface
    re_path('^api/(.*)', serve, {'server': SERVER}),

    # REST interface v1
    # Schema url
    re_path(r'^v1/schemas/$',
            views_v1.SchemaDetail.as_view()),
    # Persons urls
    re_path(r'^v1/persons/$',
            views_v1.PersonList.as_view()),
    re_path(r'^v1/persons/(?P<pk>[a-z0-9\-]+)/$',
            views_v1.PersonDetail.as_view()),
    # Project urls
    re_path(r'^v1/projects/$', views_v1.ProjectList.as_view()),
    re_path(r'^v1/projects/(?P<pk>[a-z0-9\-]+)/$',
            views_v1.ProjectDetail.as_view()),
    # Domains urls
    re_path(r'^v1/domains/$', views_v1.DomainList.as_view()),
    # Assignments urls
    re_path(r'^v1/projects/(?P<project_id>[a-z0-9\-]+)/assignments/'
            r'(?P<rolename>[A-Za-z0-9\-]+)/$',
            views_v1.AssignmentList.as_view()),
    re_path(r'^v1/projects/(?P<project_id>[a-z0-9\-]+)/assignments/'
            r'(?P<rolename>[A-Za-z0-9\-]+)/(?P<membername>[A-Za-z0-9\-]+)/$',
            views_v1.AssignmentList.as_view()),
    # Grants urls
    re_path(r'^v1/projects/(?P<project_id>[a-z0-9\-]+)/grants/$',
            views_v1.GrantList.as_view()),
    re_path(r'^v1/projects/(?P<project_id>[a-z0-9\-]+)/grants/'
            r'(?P<groupname>[A-Za-z0-9\-]+)/$',
            views_v1.GrantList.as_view()),
    # Service urls
    re_path(r'^v1/services/$', views_v1.ServiceList.as_view()),
    # ServiceAssignmentList urls
    re_path(r'^v1/projects/(?P<pk>[a-z0-9\-]+)/services/$',
            views_v1.ServiceAssignmentList.as_view()),
    re_path(r'^v1/projects/(?P<pk>[a-z0-9\-]+)/services/'
            r'(?P<servicename>[A-Za-z0-9\-]+)/$',
            views_v1.ServiceAssignmentList.as_view()),
    # Properties urls
    re_path(r'^v1/projects/(?P<pk>[a-z0-9\-]+)/properties/$',
            views_v1.PropertiesList.as_view()),
    re_path(r'^v1/projects/(?P<pk>[a-z0-9\-]+)/properties/'
            r'(?P<property_name>[A-Za-z0-9\-\._]+)/$',
            views_v1.PropertiesDetail.as_view()),

    # REST interface v2
    # Schema url
    re_path(r'^v2/schema$',
            views_v2.SchemaDetail.as_view()),
    # Project urls
    re_path(r'^v2/project$',
            views_v2.ProjectList.as_view()),
    re_path(r'^v2/project/(?P<pk>[a-z0-9\-]+)$',
            views_v2.ProjectDetail.as_view())
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns = format_suffix_patterns(urlpatterns)
