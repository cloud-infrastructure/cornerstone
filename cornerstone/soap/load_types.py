from pyws.functions.args import ListOf

Domain = {0: 'Domain',
          'id': str}

Member = {0: 'Member',
          'name': str}

Project = {0: 'Project',
           'id': str,
           'name': str,
           'description': str,
           'enabled': bool,
           'owner': str}

ServiceAllowedList = ListOf(str)

ProjectInfo = {0: 'ProjectInfo',
               'id': str,
               'name': str,
               'description': str,
               'enabled': bool,
               'owner': str,
               'allowOperators': bool,
               'allowSupporters': bool,
               'services': ServiceAllowedList}

DomainList = ListOf(Domain)
UserList = ListOf(Member)
ProjectList = ListOf(Project)
MemberList = ListOf(Member)

ReturnCode = {0: 'ReturnCode', 'returnCode': int, 'msg': str}
ReturnDomainList = {0: 'ReturnDomainList', 'returnCode': int,
                    'domainList': DomainList}
ReturnMemberList = {0: 'ReturnMemberList', 'returnCode': int,
                    'memberList': MemberList}
ReturnUserList = {0: 'ReturnUserList', 'returnCode': int,
                  'userList': UserList}
ReturnProject = {0: 'ReturnProject', 'returnCode': int,
                 'project': Project}
ReturnProjectList = {0: 'ReturnProjectList', 'returnCode': int,
                     'projectList': ProjectList}
ReturnProjectInfo = {0: 'ReturnProjectInfo', 'returnCode': int,
                     'projectInfo': ProjectInfo}
