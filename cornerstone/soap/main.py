import logging

from pyws.functions.register import register
from pyws.server import SoapServer

from cornerstone.manager import Manager
from cornerstone import settings
from cornerstone.soap.load_types import ReturnCode
from cornerstone.soap.load_types import ReturnDomainList
from cornerstone.soap.load_types import ReturnMemberList
from cornerstone.soap.load_types import ReturnProject
from cornerstone.soap.load_types import ReturnProjectInfo
from cornerstone.soap.load_types import ReturnProjectList

# Get an instance of a logger
LOGGER = logging.getLogger(__name__)

# Instanciate SOAP SERVER
SERVER = SoapServer(
    service_name=settings.SERVICE_NAME,
    tns=settings.SERVICE_LOCATION,
    location=settings.SERVICE_LOCATION,
)

CMDSERVER = Manager()


# Helper functions
def _output_domain_list(ret_code, domains):
    return {'returnCode': ret_code, 'domainList': domains}


def _output_project(ret_code, project):
    return {'returnCode': ret_code, 'project': project}


def _output_project_list(ret_code, projects):
    return {'returnCode': ret_code, 'projectList': projects}


def _output_member_list(ret_code, members):
    return {'returnCode': ret_code, 'memberList': members}


def _output_data(ret_code, msg=None):
    return {'returnCode': ret_code, 'msg': msg}


def _output_project_info(ret_code, project):
    return {'returnCode': ret_code, 'projectInfo': project}


@register('add_project_members', return_type=ReturnCode, args=((str, ),
                                                               (str, ),
                                                               (str, )))
def add_project_members(project_id, rolename, members):
    """Add the members on the project within the role."""
    try:
        LOGGER.info("Add members '%s' with role '%s' to project "
                    "'%s'", members, rolename, id)
        ret = CMDSERVER.add_grant_project(project_id, rolename, members)
        if not ret:
            LOGGER.error("Members added to project failed")
            return _output_data(1)

        LOGGER.info("Members added to project successful")
        return _output_data(0)
    except Exception as excep:
        LOGGER.exception(str(excep))
        return _output_data(-1, str(excep))


@register('allow_service', return_type=ReturnCode, args=((str, ),
                                                         (bool, ),
                                                         (str, )))
def allow_service(service_name, enabled, project_id):
    try:
        logger_str_allow = "Allow" if enabled else "Not allow"
        logger_str_allowed = "allowed" if enabled else "disabled"
        LOGGER.info("%s service '%s' in project '%s'",
                    logger_str_allow,
                    service_name,
                    project_id)
        ret = CMDSERVER.allow_service(service_name, enabled, project_id)
        if not ret:
            LOGGER.error("Allow service failed")
            return _output_data(1)

        LOGGER.info("Service %s", logger_str_allowed)
        return _output_data(0)
    except Exception as excep:
        LOGGER.exception(str(excep))
        return _output_data(-1, str(excep))


@register('del_project_members', return_type=ReturnCode, args=((str, ),
                                                               (str, ),
                                                               (str, )))
def del_project_members(project_id, rolename, members):
    """Remove the members on the project within the role."""
    try:
        LOGGER.info("Delete members '%s' from project '%s' with role %s",
                    members, project_id, rolename)
        ret = CMDSERVER.delete_grant_project(project_id, rolename, members)
        if not ret:
            LOGGER.error("Members removed from project failed")
            return _output_data(1)

        LOGGER.info("Members removed from project successful")
        return _output_data(0)
    except Exception as excep:
        LOGGER.exception(str(excep))
        return _output_data(-1, str(excep))


@register('domain_list', return_type=ReturnDomainList)
def domain_list():
    """Get list of domain id's from JSON file."""
    try:
        LOGGER.info("List domains")
        domains = CMDSERVER.domain_list()
        return _output_domain_list(0, domains)
    except IOError as excep:
        LOGGER.exception(excep.strerror + ": " + excep.filename)
        return _output_domain_list(-1, [])


@register('get_project_members', return_type=ReturnMemberList, args=((str, ),
                                                                     (str, )))
def get_project_members(project_id, rolename):
    try:
        LOGGER.info("Get members of project '%s' with role '%s'",
                    project_id, rolename)
        members = CMDSERVER.get_project_members(project_id, rolename)
        if not members:
            LOGGER.error('Members retrieval from project failed')
            return _output_member_list(0, list([{'name': 'empty'}]))

        LOGGER.info('Members retrieval from project successful')
        return _output_member_list(0, members)
    except Exception as excep:
        LOGGER.exception(str(excep))
        return _output_member_list(-1, [])


@register('get_projectInfo', return_type=ReturnProjectInfo, args=(str, ))
def get_project_info(project_id):
    """Retrieve the project identified by the id."""
    try:
        LOGGER.info("Get project info '%s'", project_id)
        project = CMDSERVER.project_getinfo(project_id)
        if not project:
            LOGGER.error('Project get info failed on "%s"', project_id)
            return _output_project(1, None)

        LOGGER.info('Retrieval of project with extra info successful')
        project['services'] = [s['service_name'] for s in project['services']]
        return _output_project_info(0, project)
    except Exception as excep:
        LOGGER.exception(str(excep))
        return _output_project(-1, None)


@register('project_create', return_type=ReturnProject, args=((str, ),
                                                             (str, ),
                                                             (str, ''),
                                                             (bool, True),
                                                             (str, )))
def project_create(id, name, description, enabled, owner):
    project_id = id
    """ Create the project
    """
    LOGGER.info("Create project: id='%s'; name='%s'; desc='%s'; enabled='%s'"
                "with owner '%s'", project_id, name, description, enabled,
                owner)
    # Check if name is empty
    if not project_id or not name:
        LOGGER.warning('Unable to create empty project')
        return _output_project(1, None)
    try:
        project = CMDSERVER.project_create(
            project_id=project_id,
            name=name,
            description=description,
            enabled=enabled,
            owner=owner)
        if not project:
            LOGGER.error('Project creation failed on "%s"',
                         project_id)
            return _output_project(-1, None)

        LOGGER.info('Project "%s" created successful', project_id)
        return _output_project(0, project)
    except Exception as excep:
        LOGGER.exception(str(excep))
        return _output_project(-1, None)


@register('project_delete', return_type=ReturnProject, args=(str, ))
def project_delete(id):
    project_id = id
    """ Delete the project identified by the id
    """
    LOGGER.info("Delete project '%s'", project_id)
    # Check if id is empty
    if not project_id:
        LOGGER.warning('Unable to delete empty id')
        return _output_project(1, None)
    try:
        # Delete it and related services
        output = CMDSERVER.project_delete(project_id)
        if not output:
            LOGGER.error('Project get failed on "%s"',
                         project_id)
            return _output_project(-1, None)

        LOGGER.info("Project '%s' deleted", project_id)
        return _output_project(0, None)
    except Exception as excep:
        LOGGER.exception(str(excep))
        return _output_project(-1, None)


@register('project_get', return_type=ReturnProject, args=(str, ))
def project_get(id):
    project_id = id
    try:
        LOGGER.info("Get project '%s'", project_id)
        if not project_id:
            LOGGER.warning('Unable to get empty id')
            return _output_project(1, None)

        project = CMDSERVER.project_get(project_id)
        if not project:
            LOGGER.error('Project get failed on "%s"',
                         project_id)
            return _output_project(1, None)

        LOGGER.info('Retrieval of project successful')
        for key in ('allowOperators', 'allowSupporters', 'services'):
            project.pop(key, None)
        return _output_project_info(0, project)
    except Exception as excep:
        LOGGER.exception(str(excep))
        return _output_project(-1, None)


@register('project_list', return_type=ReturnProjectList)
def project_list():
    try:
        LOGGER.info("List projects")
        ret = CMDSERVER.project_list()
        if not ret:
            LOGGER.error('Unable to retrieve project list')
            return _output_project_list(-1, [])

        LOGGER.info('Retrieval of project list successful')
        return _output_project_list(0, ret)
    except Exception as excep:
        LOGGER.exception(str(excep))
        return _output_project_list(-1, [])


@register('project_update', return_type=ReturnProject, args=((str, ),
                                                             (str, ),
                                                             (str, ),
                                                             (bool, ),
                                                             (str, )))
def project_update(id, name, description, enabled, owner):
    project_id = id
    try:
        LOGGER.info("Update project '%s': name='%s'; desc='%s'; enabled='%s'; "
                    "owner='%s'", project_id, name, description, enabled,
                    owner)
        if not project_id:
            LOGGER.warning('Unable to update empty id')
            return _output_project(1, None)
        project = CMDSERVER.project_update(
            project_id=project_id,
            name=name,
            description=description,
            enabled=enabled,
            owner=owner)
        if not project:
            LOGGER.error('Project update failed on "%s"',
                         project_id)
            return _output_project(-1, None)

        LOGGER.info('Project "%s" updated successful', project_id)
        return _output_project(0, project)
    except Exception as excep:
        LOGGER.exception(excep)
        return _output_project(-1, None)


@register('set_allowOperators', return_type=ReturnCode, args=((str, ),
                                                              (bool, False)))
def set_allow_operators(project_id, value):
    """Set/unset the operators role on the operators group."""
    LOGGER.info("Set to '%s' allow operators in project '%s'",
                value, project_id)

    allowed = CMDSERVER.is_allowed(project_id=project_id,
                                   group_name=settings.GROUP_OPERATORS,
                                   role_name=settings.ROLE_OPERATORS)

    if (allowed and not value) or (not allowed and value):
        try:
            ret = CMDSERVER.set_allow_group(
                project_id=project_id,
                value=value,
                group_name=settings.GROUP_OPERATORS,
                role_name=settings.ROLE_OPERATORS)
            if not ret:
                LOGGER.error('Project allow operators failed')
                return _output_data(1)

            LOGGER.info('Project "%s" set to "%s" for allow operators',
                        project_id, value)
        except Exception as excep:
            LOGGER.exception(str(excep))
            return _output_data(-1, str(excep))
    return _output_data(0)


@register('set_project_members', return_type=ReturnCode, args=((str, ),
                                                               (str, ),
                                                               (str, )))
def set_project_members(project_id, rolename, members):
    try:
        LOGGER.info("Set members '%s' with role '%s' to project '%s'",
                    members, rolename, project_id)
        ret = CMDSERVER.set_project_members(project_id, rolename, members)
        if not ret:
            LOGGER.error('Members set to project failed')
            return _output_data(1)

        LOGGER.info('Members set to project successfully')
        return _output_data(0)
    except Exception as excep:
        LOGGER.exception(str(excep))
        return _output_data(-1, str(excep))
